package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoNoticia;


public class MostrarNoticia extends BaseActivity {


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            startActivity(new Intent(context, Noticias.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_mostrar_noticia);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(user.getEmail());
        textViewNombreUsuario.setText(user.getDisplayName());

        TipoNoticia tipoNoticia = (TipoNoticia) getIntent().getExtras().getSerializable("Noticia");

        myRef = FirebaseDatabase.getInstance().getReference();

        showProgressDialog("Obteniendo información...");

        TextView titulo = findViewById(R.id.titulo);
        titulo.setText(tipoNoticia.Titulo);

        TextView autor = findViewById(R.id.autor);
        autor.setText("Autor: " + tipoNoticia.Autor);

        TextView cuerpo = findViewById(R.id.cuerpo);
        cuerpo.setText(tipoNoticia.Cuerpo);

        TextView fecha = findViewById(R.id.fecha);
        fecha.setText("Fecha: " + tipoNoticia.Fecha);

        hideProgressDialog();

    }


}
