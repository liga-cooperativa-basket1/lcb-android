package liga.cooperativa.basket.ligacooperativabasket.firebase

import android.app.DownloadManager
import android.content.pm.PackageInfo
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import liga.cooperativa.basket.ligacooperativabasket.network.GitlabClient
import liga.cooperativa.basket.ligacooperativabasket.network.GitlabRelease
import liga.cooperativa.basket.ligacooperativabasket.network.GitlabService

class InfoViewModel : ViewModel() {

    private val client: GitlabService = GitlabClient.getInstance().create(GitlabService::class.java)
    private val _newVersion = MutableLiveData<GitlabRelease?>()
    val newVersion: LiveData<GitlabRelease?>
        get() = _newVersion

    fun checkUpdates(info: PackageInfo?) {
        viewModelScope.launch {
            val lastRelease = client.getReleases().first()
            if (info?.versionName != lastRelease.tag_name) {
                _newVersion.value = lastRelease
            } else {
                _newVersion.value = null
            }
        }
    }
    fun updateVersion(downloadManager: DownloadManager, destination: String) {
        val uri = Uri.parse(
            GitlabClient.repoUrl +
                newVersion.value?.description?.substringAfterLast("(")?.substringBeforeLast(")")
        )
        val request = DownloadManager.Request(uri)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
        request.setDestinationUri(Uri.parse("file://$destination"))
        downloadManager.enqueue(request)
    }
}
