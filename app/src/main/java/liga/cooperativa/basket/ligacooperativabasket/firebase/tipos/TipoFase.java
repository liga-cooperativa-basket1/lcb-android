package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

public class TipoFase {
    public String Nombre;
    public String FechaInicio;
    public String FechaFin;
    public String Temporada;
    public int Grupos;

    public TipoFase() {

    }

}
