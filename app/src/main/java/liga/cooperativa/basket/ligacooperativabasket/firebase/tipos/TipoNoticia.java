package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import java.io.Serializable;

@SuppressWarnings("Serialize")
public class TipoNoticia implements Serializable {
    public String Titulo;
    public String Cuerpo;
    public String Fecha;
    public String Autor;
    public String AutorID;
    public String DatabaseId;

    public TipoNoticia() {

    }
}
