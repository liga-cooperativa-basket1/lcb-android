package liga.cooperativa.basket.ligacooperativabasket.firebase;

import static liga.cooperativa.basket.ligacooperativabasket.firebase.utils.TypeExtensionsKt.getTeamLogo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import liga.cooperativa.basket.ligacooperativabasket.BR;
import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.databinding.ItemTableTeamBinding;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoClasificacion;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;


public class Clasificacion extends Fragment {

    public static String GROUP = "group";
    ArrayList<ArrayList<TipoEquipo>> equiposPorGrupo = new ArrayList<>();
    private ArrayList<TipoEquipo> arrayListTipoEquipos = new ArrayList<>();
    private RecyclerView recyclerView;
    private TipoEquipo miEquipo;
    private int group;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_clasificacion, container, false);

        if (getArguments() != null) {
            group = getArguments().getInt(GROUP);
        }

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        recyclerView = view.findViewById(R.id.recyclerView);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                showProgressDialog("Actualizando clasificación...");

                equiposPorGrupo = new ArrayList<>();
                arrayListTipoEquipos = new ArrayList<>();

                if (!dataSnapshot.child("Equipos").exists()) {
                    Toast.makeText(getContext(), "No existen equipos", Toast.LENGTH_LONG).show();
                } else if (dataSnapshot.child("Equipos").getChildrenCount() > 0) {

                    String miEquipoSt = (String) dataSnapshot.child("Usuarios").child(mAuth.getUid()).child("Equipo").getValue();

                    Iterable<DataSnapshot> iterable = dataSnapshot.child("Equipos").getChildren();

                    for (DataSnapshot ds : iterable) {
                        TipoEquipo e = ds.getValue(TipoEquipo.class);
                        if (e.Grupo != 0) {
                            e.DataBaseID = ds.getKey();
                            if (e.Clasificacion == null) {
                                e.Clasificacion = new TipoClasificacion();
                            }
                            e.Clasificacion.Puntos = e.Clasificacion.Victorias * 2 + e.Clasificacion.Derrotas;
                            arrayListTipoEquipos.add(e);
                            if (e.Nombre.equals(miEquipoSt))
                                miEquipo = e;
                        }
                    }


                    ArrayList<TipoPartido> partidos = new ArrayList<>();
                    iterable = dataSnapshot.child("Partidos").getChildren();

                    for (DataSnapshot ds : iterable) {
                        TipoPartido tp = ds.getValue(TipoPartido.class);
                        partidos.add(tp);
                    }


                    equiposPorGrupo = TipoClasificacion.separaEquiposPorGrupo(arrayListTipoEquipos, partidos);

                    new LastAdapter(equiposPorGrupo.get(group), BR.team)
                            .map(TipoEquipo.class, new ItemType<ItemTableTeamBinding>(R.layout.item_table_team) {
                                @Override
                                public void onBind(@NotNull Holder<ItemTableTeamBinding> holder) {
                                    TipoEquipo team = equiposPorGrupo.get(group).get(holder.getBindingAdapterPosition());
                                    holder.getBinding().setPosition(equiposPorGrupo.get(group).indexOf(team) + 1);
                                    holder.getBinding().setLogo(getTeamLogo(dataSnapshot, FirebaseStorage.getInstance(), team.Nombre));
                                    holder.itemView.setOnClickListener(view -> {
                                        TipoEquipo e = equiposPorGrupo.get(group).get(holder.getBindingAdapterPosition());
                                        Intent intent = new Intent(getContext(), MostrarEquipo.class);
                                        intent.putExtra("Equipo", e);
                                        startActivity(intent);
                                    });
                                }
                            })
                            .into(recyclerView);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }
}
