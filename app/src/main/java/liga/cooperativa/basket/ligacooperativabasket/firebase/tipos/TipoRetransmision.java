package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import java.io.Serializable;

@SuppressWarnings("Serializable")
public class TipoRetransmision implements Serializable {

    public String Local;
    public String Visitante;
    public int ResultadoLocal;
    public int ResultadoVisitante;
    public String Tiempo;
    public String Cuarto;
    public int FaltasLocal;
    public int FaltasVisitante;
    public String DatabaseID;

    public TipoRetransmision() {

    }

}
