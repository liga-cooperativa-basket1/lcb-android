package liga.cooperativa.basket.ligacooperativabasket.firebase;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import liga.cooperativa.basket.ligacooperativabasket.R;

public class ObtenerDatosGoogle extends BaseActivity {

    FloatingActionButton fab;
    Context context = this;


    public String getAccount() {
        // This call requires the Android GET_ACCOUNTS permission
        Account[] accounts = AccountManager.get( context).
                getAccountsByType("com.google");
        if (accounts.length == 0) {
            return null;
        }
        return accounts[0].name;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_empty);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();

        final String accountName = getAccount();


        // Initialize the scope using the client ID you got from the Console.
        final String scope = "audience:server:client_id:"
                + "383828146959-lf19m6qdkmmjj1cujurhrp55jorc4saf.apps.googleusercontent.com";

        showProgressDialog("Cargando datos necesarios...");

        new Thread(new Runnable() {
            public void run() {
                try {
                    String idToken = GoogleAuthUtil.getToken(context, accountName, scope);
                    myRef.child("Usuarios").child(mAuth.getUid()).child("TokenFCM").setValue(idToken);
                    hideProgressDialog();

                    /*String hola = addToGroup("prueba","NoticiasLigaCooperativa",idToken,idToken);*/

                    send();

                    startActivity(new Intent(context, MainActivity.class));
                } catch (Exception e) {
                    int a = 2;
                }
            }
        }).start();
    }

    public void send()
    {
        try {
            HttpURLConnection httpcon = (HttpURLConnection) ((new URL("https://fcm.googleapis.com/fcm/send").openConnection()));
            httpcon.setDoOutput(true);
            httpcon.setRequestProperty("Content-Type", "application/json");
            httpcon.setRequestProperty("Authorization", "key=AIzaSyCJY0SmRXGkAMqx7b0_GROkheWsZB1miyo");
            httpcon.setRequestMethod("POST");
            httpcon.connect();
            System.out.println("Connected!");

            byte[] outputBytes = "{\"notification\":{\"title\": \"My title\", \"text\": \"My text\", \"sound\": \"default\"}, \"to\": \"eyJhbGciOiJSUzI1NiIsImtpZCI6IjA3YTA4MjgzOWYyZTcxYTliZjZjNTk2OTk2Yjk0NzM5Nzg1YWZkYzMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMzgzODI4MTQ2OTU5LXZzdmRqanQycmY1dXQzNGphbmJjYmFrb2I2cWJuM3J1LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMzgzODI4MTQ2OTU5LWxmMTltNnFka21tamoxY3VqdXJocnA1NWpvcmM0c2FmLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTEzODQ2NDQ1NDMxMzk4MDM0NDc3IiwiZW1haWwiOiJqb3JnZWNhbmFkaWVuc2VyZWFsQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpYXQiOjE1NTg4NTkyMTksImV4cCI6MTU1ODg2MjgxOX0.ltouB-jetHoXC8umoZXdQfWPGVCdsm150fG8_-6Qj4Ae0ru6eN1bJ6_D52517H133tAaXPF04xek1CcLhriiyhD5WzfhysYLoWrJBcR3g5FrExFo4UVxJ21E3HcjU9M6Jp_pchEfZaGQGjtGYJiS9ZXRC0kOhue5fcbSpdMON3lbGda-uCC4zLmDBlO30OqfFjivW6Vk16z81cPahqOyn9JwXH9B8wtpxdrEMYiKoKnzYv2lVyeRFnEQ_fi-9XlsgD2h2CScrUr0JtwtwKGWgDCTC6mN6yAZOyRqC7-v683KwVYSzdbjeCO4S3bHABDE7_iibTgYx5gbLLIJwX2lsA\"}".getBytes("UTF-8");
            OutputStream os = httpcon.getOutputStream();
            os.write(outputBytes);
            os.close();

            // Reading response
            InputStream input = httpcon.getInputStream();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                for (String line; (line = reader.readLine()) != null;) {
                    System.out.println(line);
                }
            }
            catch(Exception ex)
            {
                int a = 2;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
