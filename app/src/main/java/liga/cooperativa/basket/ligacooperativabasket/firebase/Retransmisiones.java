package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.widget.RadioButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoRetransmision;

public class Retransmisiones extends BaseActivity {

    private final Context context = this;
    TextView txtViewNombreLocal;
    TextView textViewMarcadorLocal;
    RadioButton radioButtonFaltaUnoLocal;
    RadioButton radioButtonFaltaDosLocal;
    RadioButton radioButtonFaltaTresLocal;
    RadioButton radioButtonFaltaCuatroLocal;
    RadioButton radioButtonFaltaCincoLocal;
    TextView textViewTiempoPartido;
    TextView textViewCuartoPartido;
    TextView txtViewNombreVisitante;
    TextView textViewMarcadorVisitante;
    RadioButton radioButtonFaltaUnoVisitante;
    RadioButton radioButtonFaltaDosVisitante;
    RadioButton radioButtonFaltaTresVisitante;
    RadioButton radioButtonFaltaCuatroVisitante;
    RadioButton radioButtonFaltaCincoVisitante;

    TipoPartido partido;
    TipoRetransmision retransmision;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            startActivity(new Intent(context, MainActivity.class));
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_retransmision);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
//        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(mAuth.getCurrentUser().getEmail());
        textViewNombreUsuario.setText(mAuth.getCurrentUser().getDisplayName());

        partido = (TipoPartido) getIntent().getExtras().getSerializable("Partido");

        txtViewNombreLocal = findViewById(R.id.txtViewNombreLocal);
        textViewMarcadorLocal = findViewById(R.id.textViewMarcadorLocal);
        textViewTiempoPartido = findViewById(R.id.textViewTiempoPartido);
        textViewCuartoPartido = findViewById(R.id.textViewCuartoPartido);
        txtViewNombreVisitante = findViewById(R.id.txtViewNombreVisitante);
        textViewMarcadorVisitante = findViewById(R.id.textViewMarcadorVisitante);

        radioButtonFaltaUnoLocal = findViewById(R.id.radioButtonFaltaUnoLocal);
        radioButtonFaltaDosLocal = findViewById(R.id.radioButtonFaltaDosLocal);
        radioButtonFaltaTresLocal = findViewById(R.id.radioButtonFaltaTresLocal);
        radioButtonFaltaCuatroLocal = findViewById(R.id.radioButtonFaltaCuatroLocal);
        radioButtonFaltaCincoLocal = findViewById(R.id.radioButtonFaltaCincoLocal);
        radioButtonFaltaUnoVisitante = findViewById(R.id.radioButtonFaltaUnoVisitante);
        radioButtonFaltaDosVisitante = findViewById(R.id.radioButtonFaltaDosVisitante);
        radioButtonFaltaTresVisitante = findViewById(R.id.radioButtonFaltaTresVisitante);
        radioButtonFaltaCuatroVisitante = findViewById(R.id.radioButtonFaltaCuatroVisitante);
        radioButtonFaltaCincoVisitante = findViewById(R.id.radioButtonFaltaCincoVisitante);

        txtViewNombreLocal.setText(partido.Local);
        txtViewNombreVisitante.setText(partido.Visitante);


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                /*if (!dataSnapshot.child("Retransmisiones").child(partido.DataBaseID).exists()) {
                    Toast.makeText(context, "No existen partidos en directo en este momento", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                }*/
                retransmision = dataSnapshot.child("Retransmisiones").child(partido.DataBaseID).getValue(TipoRetransmision.class);
                if (retransmision != null) {
                    textViewTiempoPartido.setText(retransmision.Tiempo);
                    textViewCuartoPartido.setText(retransmision.Cuarto);
                    textViewMarcadorLocal.setText("" + retransmision.ResultadoLocal);
                    textViewMarcadorVisitante.setText("" + retransmision.ResultadoVisitante);

                    switch (retransmision.FaltasLocal) {
                        case 5:
                            radioButtonFaltaCincoLocal.setChecked(true);
                            radioButtonFaltaCuatroLocal.setChecked(true);
                            radioButtonFaltaTresLocal.setChecked(true);
                            radioButtonFaltaDosLocal.setChecked(true);
                            radioButtonFaltaUnoLocal.setChecked(true);
                            break;
                        case 4:
                            radioButtonFaltaCincoLocal.setChecked(false);
                            radioButtonFaltaCuatroLocal.setChecked(true);
                            radioButtonFaltaTresLocal.setChecked(true);
                            radioButtonFaltaDosLocal.setChecked(true);
                            radioButtonFaltaUnoLocal.setChecked(true);
                            break;
                        case 3:
                            radioButtonFaltaCincoLocal.setChecked(false);
                            radioButtonFaltaCuatroLocal.setChecked(false);
                            radioButtonFaltaTresLocal.setChecked(true);
                            radioButtonFaltaDosLocal.setChecked(true);
                            radioButtonFaltaUnoLocal.setChecked(true);
                            break;
                        case 2:
                            radioButtonFaltaCincoLocal.setChecked(false);
                            radioButtonFaltaCuatroLocal.setChecked(false);
                            radioButtonFaltaTresLocal.setChecked(false);
                            radioButtonFaltaDosLocal.setChecked(true);
                            radioButtonFaltaUnoLocal.setChecked(true);
                            break;
                        case 1:
                            radioButtonFaltaCincoLocal.setChecked(false);
                            radioButtonFaltaCuatroLocal.setChecked(false);
                            radioButtonFaltaTresLocal.setChecked(false);
                            radioButtonFaltaDosLocal.setChecked(false);
                            radioButtonFaltaUnoLocal.setChecked(true);
                            break;
                        case 0:
                            radioButtonFaltaCincoLocal.setChecked(false);
                            radioButtonFaltaCuatroLocal.setChecked(false);
                            radioButtonFaltaTresLocal.setChecked(false);
                            radioButtonFaltaDosLocal.setChecked(false);
                            radioButtonFaltaUnoLocal.setChecked(false);
                            break;
                    }

                    switch (retransmision.FaltasVisitante) {
                        default:
                        case 5:
                            radioButtonFaltaCincoVisitante.setChecked(true);
                            radioButtonFaltaCuatroVisitante.setChecked(true);
                            radioButtonFaltaTresVisitante.setChecked(true);
                            radioButtonFaltaDosVisitante.setChecked(true);
                            radioButtonFaltaUnoVisitante.setChecked(true);
                            break;
                        case 4:
                            radioButtonFaltaCincoVisitante.setChecked(false);
                            radioButtonFaltaCuatroVisitante.setChecked(true);
                            radioButtonFaltaTresVisitante.setChecked(true);
                            radioButtonFaltaDosVisitante.setChecked(true);
                            radioButtonFaltaUnoVisitante.setChecked(true);
                            break;
                        case 3:
                            radioButtonFaltaCincoVisitante.setChecked(false);
                            radioButtonFaltaCuatroVisitante.setChecked(false);
                            radioButtonFaltaTresVisitante.setChecked(true);
                            radioButtonFaltaDosVisitante.setChecked(true);
                            radioButtonFaltaUnoVisitante.setChecked(true);
                            break;
                        case 2:
                            radioButtonFaltaCincoVisitante.setChecked(false);
                            radioButtonFaltaCuatroVisitante.setChecked(false);
                            radioButtonFaltaTresVisitante.setChecked(false);
                            radioButtonFaltaDosVisitante.setChecked(true);
                            radioButtonFaltaUnoVisitante.setChecked(true);
                            break;
                        case 1:
                            radioButtonFaltaCincoVisitante.setChecked(false);
                            radioButtonFaltaCuatroVisitante.setChecked(false);
                            radioButtonFaltaTresVisitante.setChecked(false);
                            radioButtonFaltaDosVisitante.setChecked(false);
                            radioButtonFaltaUnoVisitante.setChecked(true);
                            break;
                        case 0:
                            radioButtonFaltaCincoVisitante.setChecked(false);
                            radioButtonFaltaCuatroVisitante.setChecked(false);
                            radioButtonFaltaTresVisitante.setChecked(false);
                            radioButtonFaltaDosVisitante.setChecked(false);
                            radioButtonFaltaUnoVisitante.setChecked(false);
                            break;
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
