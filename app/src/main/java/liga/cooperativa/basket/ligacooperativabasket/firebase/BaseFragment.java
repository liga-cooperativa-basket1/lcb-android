package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import liga.cooperativa.basket.ligacooperativabasket.R;

public class BaseFragment extends Fragment {

    private static final String SCREEN = "HEAD";

    public static BaseFragment newInstance(String screen) {
        BaseFragment frag = new BaseFragment();

        Bundle args = new Bundle();
        args.putString(SCREEN, screen);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {

        View layout = null;
        switch (getArguments().getString(SCREEN)) {
            case "Calendario":
                layout = inflater.inflate(R.layout.fragment_calendario, container, false);
                break;
            /*case "Gallery":
                layout = inflater.inflate(R.layout.fragment_gallery, container, false);
                break;
            case "Tools":
                layout = inflater.inflate(R.layout.fragment_tools, container, false);
                break;
            case "Share":
                layout = inflater.inflate(R.layout.fragment_share, container, false);
                break;
            case "Send":
                layout = inflater.inflate(R.layout.fragment_send, container, false);

                if (getArguments() != null) {
                    ((TextView) layout.findViewById(R.id.text11)).setText(getArguments().getString(TEXT1));
                    ((TextView) layout.findViewById(R.id.text12)).setText(getArguments().getString(TEXT2));
                }

                break;*/
            default:
                throw new IllegalArgumentException("menu option not implemented!!");
        }

        return layout;

        /*View layout = inflater.inflate(R.layout.home_fragment2, container, false);

        if (getArguments() != null) {
            ((TextView) layout.findViewById(R.id.text11)).setText(getArguments().getString(TEXT1));
            ((TextView) layout.findViewById(R.id.text12)).setText(getArguments().getString(TEXT2));
        }

        return layout;*/
    }
}

