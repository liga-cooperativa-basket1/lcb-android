package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@SuppressWarnings("Serializable")
public class TipoClasificacion implements Serializable {
    public int CanastasFavor;
    public int CanastasContra;
    public int Derrotas;
    public int Victorias;
    public int Puntos;

    public TipoClasificacion() {
    }

    public static ArrayList<ArrayList<TipoEquipo>> calculaClasificacionCambioFase(ArrayList<ArrayList<TipoEquipo>> equipos, ArrayList<TipoPartido> partidos) {
        for (ArrayList<TipoEquipo> ae : equipos) {
            for (TipoEquipo e : ae) {
                e.Clasificacion = TipoClasificacion.obtenClasificacionEquipo(e, partidos);
            }
        }


        return equipos;
    }

    /**
     * @param equipo   Equipo para el cual obtener la clasificación
     * @param partidos Lista de partidos
     * @return TipoClasificacion del equipo en cuestion
     */
    public static TipoClasificacion obtenClasificacionEquipo(TipoEquipo equipo, ArrayList<TipoPartido> partidos) {
        TipoClasificacion c = new TipoClasificacion();
        ArrayList<TipoPartido> partidosEquipo = TipoPartido.obtenerPartidosEquipo(equipo, partidos);
        for (TipoPartido p : partidosEquipo) {
            if (!p.ResultadoLocal.equals("SIN DEFINIR") && !p.ResultadoVisitante.equals("SIN DEFINIR")) {
                if (equipo.Nombre.equals(p.Local)) //LOCAL
                {
                    if (Integer.parseInt(p.ResultadoLocal) > Integer.parseInt(p.ResultadoVisitante)) //VICTORIA
                        c.Victorias++;
                    else //DERROTA
                        c.Derrotas++;
                    c.CanastasFavor += Integer.parseInt(p.ResultadoLocal);
                    c.CanastasContra += Integer.parseInt(p.ResultadoVisitante);
                } else //VISITANTE
                {
                    if (Integer.parseInt(p.ResultadoVisitante) > Integer.parseInt(p.ResultadoLocal)) //VICTORIA
                        c.Victorias++;
                    else //DERROTA
                        c.Derrotas++;

                    c.CanastasFavor += Integer.parseInt(p.ResultadoVisitante);
                    c.CanastasContra += Integer.parseInt(p.ResultadoLocal);
                }
            }
        }
        c.Puntos = (2 * c.Victorias + c.Derrotas);

        return c;
    }

    public static ArrayList<ArrayList<TipoEquipo>> separaEquiposPorGrupo(ArrayList<TipoEquipo> tipoEquipos, final ArrayList<TipoPartido> partidos) {
        ArrayList<ArrayList<TipoEquipo>> equiposGrupos = new ArrayList<>();
        ArrayList<ArrayList<TipoEquipo>> equiposGruposOrdenados = new ArrayList<>();
        ArrayList<Integer> grupos = new ArrayList<>();

        for (TipoEquipo e : tipoEquipos) {
            if (e.Grupo == 0)
                continue;
            if (grupos.indexOf(e.Grupo) == -1)
                grupos.add(e.Grupo);
        }

        Collections.sort(grupos, new Comparator<Integer>() {
            @Override
            public int compare(Integer primero, Integer segundo) {
                return primero - segundo;
            }
        });

        for (int grupo : grupos) {
            equiposGrupos.add(new ArrayList<TipoEquipo>());
        }


        for (TipoEquipo e : tipoEquipos) {
            if (e.Grupo == 0)
                continue;
            equiposGrupos.get(e.Grupo - 1).add(e);
        }

        for (ArrayList<TipoEquipo> ae : equiposGrupos)
            equiposGruposOrdenados.add(ordenarClasificatorio(ae, partidos));

        return equiposGruposOrdenados;
    }

    public static ArrayList<TipoEquipo> ordenarClasificatorio(ArrayList<TipoEquipo> equiposGrupo, ArrayList<TipoPartido> partidos) {
        ArrayList<TipoEquipo> equiposOrdenados = new ArrayList<>();
        for (TipoEquipo e : equiposGrupo) {
            if (e.Clasificacion == null) {
                e.Clasificacion = TipoClasificacion.obtenClasificacionEquipo(e, new ArrayList<>());
            }
            e.Clasificacion.Puntos = (2 * e.Clasificacion.Victorias + e.Clasificacion.Derrotas);
        }


        while (equiposGrupo.size() > 0) {
            TipoEquipo equipoSeleccionado = equiposGrupo.get(0);
            equiposGrupo.remove(0);
            for (int i = 0; i < equiposGrupo.size(); i++) {
                if (equipoSeleccionado.Clasificacion.Puntos < equiposGrupo.get(i).Clasificacion.Puntos) {
                    equiposGrupo.add(equipoSeleccionado);
                    equipoSeleccionado = equiposGrupo.get(i);
                    equiposGrupo.remove(equipoSeleccionado);
                    i--;
                } else if (equipoSeleccionado.Clasificacion.Puntos == equiposGrupo.get(i).Clasificacion.Puntos) {
                    if (equipoSeleccionado.Clasificacion.Victorias < equiposGrupo.get(i).Clasificacion.Victorias) {
                        equiposGrupo.add(equipoSeleccionado);
                        equipoSeleccionado = equiposGrupo.get(i);
                        equiposGrupo.remove(equipoSeleccionado);
                        i--;
                    } else if (equipoSeleccionado.Clasificacion.Victorias == equiposGrupo.get(i).Clasificacion.Victorias) {
                        if (equipoSeleccionado.Clasificacion.Derrotas > equiposGrupo.get(i).Clasificacion.Derrotas) {
                            equiposGrupo.add(equipoSeleccionado);
                            equipoSeleccionado = equiposGrupo.get(i);
                            equiposGrupo.remove(equipoSeleccionado);
                            i--;
                        } else if (equipoSeleccionado.Clasificacion.Derrotas == equiposGrupo.get(i).Clasificacion.Derrotas) {
                            if (equipoSeleccionado.Clasificacion.CanastasFavor < equiposGrupo.get(i).Clasificacion.CanastasFavor) {
                                equiposGrupo.add(equipoSeleccionado);
                                equipoSeleccionado = equiposGrupo.get(i);
                                equiposGrupo.remove(equipoSeleccionado);
                                i--;
                            } else if (equipoSeleccionado.Clasificacion.CanastasFavor == equiposGrupo.get(i).Clasificacion.CanastasFavor) {
                                if (equipoSeleccionado.Clasificacion.CanastasContra > equiposGrupo.get(i).Clasificacion.CanastasContra) {
                                    equiposGrupo.add(equipoSeleccionado);
                                    equipoSeleccionado = equiposGrupo.get(i);
                                    equiposGrupo.remove(equipoSeleccionado);
                                    i--;
                                } else if (equipoSeleccionado.Clasificacion.CanastasContra == equiposGrupo.get(i).Clasificacion.CanastasContra) {
                                    TipoPartido p = TipoPartido.obtenerPartidoRivales(equipoSeleccionado.Nombre, equiposGrupo.get(i).Nombre, partidos);
                                    if (!(p == null || !p.Status.equals("JUGADO"))) {
                                        if (equipoSeleccionado.Nombre.equals(p.Local) && (Integer.parseInt(p.ResultadoLocal) < Integer.parseInt(p.ResultadoVisitante))) {
                                            equiposGrupo.add(equipoSeleccionado);
                                            equipoSeleccionado = equiposGrupo.get(i);
                                            equiposGrupo.remove(equipoSeleccionado);
                                            i--;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            equiposOrdenados.add(equipoSeleccionado);
        }
        return equiposOrdenados;
    }

}
