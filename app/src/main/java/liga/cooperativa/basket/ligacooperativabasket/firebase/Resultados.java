package liga.cooperativa.basket.ligacooperativabasket.firebase;

import static liga.cooperativa.basket.ligacooperativabasket.firebase.utils.TypeExtensionsKt.getTeamLogo;

import android.content.Intent;
import android.os.Bundle;

import com.afollestad.materialdialogs.LayoutMode;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.bottomsheets.BasicGridItem;
import com.afollestad.materialdialogs.bottomsheets.BottomSheet;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import kotlin.Unit;
import liga.cooperativa.basket.ligacooperativabasket.BR;
import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.databinding.ItemGameBinding;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.GameOptionItem;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoValoracion;
import liga.cooperativa.basket.ligacooperativabasket.firebase.utils.MaterialDialogsKt;
import liga.cooperativa.basket.ligacooperativabasket.firebase.utils.TypeExtensionsKt;


public class Resultados extends Fragment {

    private ArrayList<TipoPartido> arrayListTipoPartido = new ArrayList<>();
    private RecyclerView recyclerView;
    public FirebaseAuth mAuth;
    public Iterable<DataSnapshot> iterable;
    public String miEquipoSt;
    public DatabaseReference myRef;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_resultados, container, false);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), FijarPartidoRival.class));
            }
        });

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
//                showProgressDialog("Actualizando...");

                recyclerView = view.findViewById(R.id.recyclerView);
                arrayListTipoPartido = new ArrayList<>();

                miEquipoSt = (String) dataSnapshot.child("Usuarios").child(mAuth.getUid()).child("Equipo").getValue();

                iterable = dataSnapshot.child("Partidos").getChildren();


                for (DataSnapshot ds : iterable) {
                    TipoPartido tp = ds.getValue(TipoPartido.class);
                    if(tp.Valoracion == null)
                        tp.Valoracion = new TipoValoracion();

                    if ((tp.Local.equals(miEquipoSt) || tp.Visitante.equals(miEquipoSt)) &&
                            tp.Status.equals("FIJADO") ||
                            (tp.Local.equals(miEquipoSt) && tp.Valoracion.Visitante.equals("SIN DEFINIR")) ||
                            (tp.Visitante.equals(miEquipoSt) && tp.Valoracion.Local.equals("SIN DEFINIR"))) {
                        tp.DataBaseID = ds.getKey();
                        arrayListTipoPartido.add(tp);
                    }
                }

                if (arrayListTipoPartido.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    TextView textoSinPartidos = view.findViewById(R.id.textoSinPartidos);
                    textoSinPartidos.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    TextView textoSinPartidos = view.findViewById(R.id.textoSinPartidos);
                    textoSinPartidos.setVisibility(View.GONE);
                }

                Collections.sort(arrayListTipoPartido, (p2, p1) ->
                        p1.getMatchDate().compareTo(p2.getMatchDate()) * -1
                );


                new LastAdapter(TypeExtensionsKt.withHeaders(arrayListTipoPartido), BR.game)
                        .map(String.class, R.layout.item_date)
                        .map(TipoPartido.class, new ItemType<ItemGameBinding>(R.layout.item_game) {
                            @Override
                            public void onBind(@NotNull Holder<ItemGameBinding> holder) {
                                Object item = TypeExtensionsKt.withHeaders(arrayListTipoPartido).get(holder.getBindingAdapterPosition());
                                if (item instanceof TipoPartido) {
                                    TipoPartido game = (TipoPartido) item;
                                    holder.getBinding().setLogoHome(getTeamLogo(dataSnapshot, FirebaseStorage.getInstance(), game.Local));
                                    holder.getBinding().setLogoAway(getTeamLogo(dataSnapshot, FirebaseStorage.getInstance(), game.Visitante));
                                    holder.itemView.setOnClickListener(view -> displayGameOption(game));
                                }
                            }
                        })
                        .into(recyclerView);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return view;
    }

    private void displayGameOption(TipoPartido game) {
        if (game.Status.equals("JUGADO")) {
            evaluateTeam(game);
        } else {
            List<GameOptionItem> options = new ArrayList<>();
            Collections.addAll(options,
                    new GameOptionItem(getString(R.string.marcador_electronico), () -> {
                        Intent i = new Intent(getContext(), MarcadorElectronico.class);
                        i.putExtra("Partido", game);
                        startActivity(i);
                        return Unit.INSTANCE;
                    }),
                    new GameOptionItem(getString(R.string.introducir_resultado), () -> {
                        Intent intentIntroducirResultado = new Intent(getContext(), IntroducirResultadoPartido.class);
                        intentIntroducirResultado.putExtra("Partido", game);
                        startActivity(intentIntroducirResultado);
                        return Unit.INSTANCE;
                    }),
                    new GameOptionItem(getString(R.string.valorar_rival), () -> {
                        evaluateTeam(game);
                        return Unit.INSTANCE;
                    }),
                    new GameOptionItem(getString(R.string.editar_partido), () -> {
                        Intent intentCambiarCancha = new Intent(getContext(), ElegirCanchaPartido.class);

                        Calendar gameDate = Calendar.getInstance();
                        gameDate.setTime(game.getMatchDate());

                        if (gameDate.after(Calendar.getInstance())) {
                            intentCambiarCancha.putExtra("Fecha", gameDate);
                            intentCambiarCancha.putExtra("Local", game.Local);
                            intentCambiarCancha.putExtra("Visitante", game.Visitante);
                            intentCambiarCancha.putExtra("DatabaseID", game.DataBaseID);
                            startActivity(intentCambiarCancha);
                        } else
                            Toast.makeText(getContext(), "No se puede cambiar un partido anterior a hoy", Toast.LENGTH_LONG).show();
                        return Unit.INSTANCE;
                    }),
                    new GameOptionItem(getString(R.string.borrar_partido), () -> {
                        if (game.AutorID.equals(mAuth.getUid())) {
                            myRef.child("Partidos").child(game.DataBaseID).removeValue();
                            Toast.makeText(getContext(), "Partido borrado correctamente", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getContext(), "Únicamente puedes borrar los partidos creados por ti", Toast.LENGTH_SHORT).show();
                        return Unit.INSTANCE;
                    })
            );

            MaterialDialogsKt.gameOptionBottomSheet(
                    new MaterialDialog(requireContext(), new BottomSheet(LayoutMode.WRAP_CONTENT)),
                    options
            );

        }
    }

    private void evaluateTeam(TipoPartido game) {
        if (!game.Status.equals("FIJADO")) {
            if (game.Local.equals(miEquipoSt)) {
                if (game.Valoracion.Visitante.equals("SIN DEFINIR")) {
                    Intent intent = new Intent(getContext(), Valoracion.class);
                    intent.putExtra("Partido", game);
                    startActivity(intent);
                } else
                    Toast.makeText(getContext(), "Ya has valorado a este rival", Toast.LENGTH_LONG).show();
            } else if (game.Visitante.equals(miEquipoSt)) {
                if (game.Valoracion.Local.equals("SIN DEFINIR")) {
                    Intent intent = new Intent(getContext(), Valoracion.class);
                    intent.putExtra("Partido", game);
                    startActivity(intent);
                } else
                    Toast.makeText(getContext(), "Ya has valorado a este rival", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getContext(), "No podrás valorar a tu rival hasta jugar el partido", Toast.LENGTH_LONG).show();
        }
    }
}
