package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import java.io.Serializable;

/**
 * Created by jorge on 06/10/2018.
 */

public class TipoValoracion implements Serializable {

    public String Local;
    public String Visitante;

    public TipoValoracion()
    {
        this.Local = "SIN DEFINIR";
        this.Visitante = "SIN DEFINIR";
    }

    public Boolean isCompleted() {
        return !Local.equals("SIN DEFINIR") && !Visitante.equals("SIN DEFINIR");
    }

}
