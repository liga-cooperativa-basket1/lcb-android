package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoCancha;
import liga.cooperativa.basket.ligacooperativabasket.listados.AdaptadorListas;


public class Canchas extends BaseActivity {

    private ArrayList<TipoCancha> listaTipoCanchas = new ArrayList<>();
    private ListView listViewCanchas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_canchas);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        myRef = FirebaseDatabase.getInstance().getReference();


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //showProgressDialog("Actualizando");
                listaTipoCanchas = new ArrayList<>();

                if (dataSnapshot.child("Canchas").exists() && dataSnapshot.child("Canchas").getChildrenCount() > 0) {
                    iterable = dataSnapshot.child("Canchas").getChildren();

                    for (DataSnapshot ds : iterable) {
                        TipoCancha c = ds.getValue(TipoCancha.class);
                        listaTipoCanchas.add(c);
                    }

                    Collections.sort(listaTipoCanchas, new Comparator<TipoCancha>() {
                        @Override
                        public int compare(TipoCancha tipoCancha, TipoCancha t1) {
                            return tipoCancha.Nombre.compareTo(t1.Nombre);
                        }
                    });


                    listViewCanchas = findViewById(R.id.listaCanchas);
                    listViewCanchas.setAdapter(new AdaptadorListas(context, R.layout.elem_lista_canchas, listaTipoCanchas) {
                        @Override
                        public void onEntrada(Object entrada, View view) {
                            if (entrada != null) {
                                try {

                                    TipoCancha tipoCancha = (TipoCancha) entrada;

                                    String nombre = tipoCancha.Nombre;
                                    int numeroCanchas = tipoCancha.NumeroCanchas;
                                    int iluminacion = tipoCancha.Iluminacion;
                                    String horarioIluminacion = tipoCancha.HorarioIluminacion;
                                    String direccion = tipoCancha.Direccion;
                                    String metro = tipoCancha.Metro;
                                    String bus = tipoCancha.Bus;
                                    String informacion = tipoCancha.Informacion;
                                    String estado = tipoCancha.Estado;
                                    final String informe = tipoCancha.InformeCanchas;

                                    TextView nombreCardView = view.findViewById(R.id.nombreCardView);
                                    if (nombreCardView != null)
                                        nombreCardView.setText(nombre);

                                    TextView direccionCardView = view.findViewById(R.id.direccionCardView);
                                    if (direccionCardView != null)
                                        direccionCardView.setText(direccion);

                                    TextView iluminacionCardView = view.findViewById(R.id.iluminacionCardView);
                                    if (iluminacionCardView != null)
                                        iluminacionCardView.setText("Iluminacion: " + iluminacion + "\nHorario iluminacion: " + horarioIluminacion);

                                    TextView metroCardView = view.findViewById(R.id.metroCardView);
                                    if (metroCardView != null)
                                        metroCardView.setText(metro);

                                    TextView busCardView = view.findViewById(R.id.busCardView);
                                    if (busCardView != null)
                                        busCardView.setText(bus);

                                    TextView informacionCardView = view.findViewById(R.id.informacionCardView);
                                    if (informacionCardView != null)
                                        informacionCardView.setText("Numero canchas: " + numeroCanchas + "\nInformacion: " + informacion);

                                    TextView estadoCardView = view.findViewById(R.id.estadoCardView);
                                    if (estadoCardView != null)
                                        estadoCardView.setText(estado);

                                    TextView informeCardView = view.findViewById(R.id.informeCardView);
                                    if (informeCardView != null) {
                                        informeCardView.setTextColor(getResources().getColor(R.color.black));
                                        if (informe != null && informe.compareTo("--") != 0) {
                                            informeCardView.setText("Descargar");
                                            informeCardView.setTextColor(getResources().getColor(R.color.linkDescarga));
                                            informeCardView.setOnClickListener(new View.OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    String urlInforme = informe;
                                                    if (urlInforme.startsWith("\t\r\n"))
                                                        urlInforme = urlInforme.replace("\t\r\n", "");
                                                    Uri uri = Uri.parse(urlInforme);
                                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                                    startActivity(intent);
                                                }
                                            });
                                        } else
                                            informeCardView.setText("Sin informe");
                                    }


                                } catch (Exception ex) {
                                    Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });
                    hideProgressDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
