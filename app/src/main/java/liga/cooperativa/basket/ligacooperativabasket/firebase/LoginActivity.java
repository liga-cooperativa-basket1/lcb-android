package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.databinding.LoginLayoutBinding;

public class LoginActivity extends BaseActivity {

    private LoginLayoutBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.login_layout);
        binding.setIsLogin(true);
        mAuth = FirebaseAuth.getInstance();

        EditText email = binding.userEditText;
        EditText password = binding.userPassword;

        binding.loginButton.setOnClickListener(view ->  {
            if (binding.getIsLogin()) {
                signInUser(email.getText().toString(), password.getText().toString());
            } else {
                binding.setIsLogin(true);
            }
        });

        binding.createAccount.setOnClickListener(view -> {
            if (!binding.getIsLogin()) {
                createUser(email.getText().toString(), password.getText().toString());
            } else {
                binding.setIsLogin(false);
            }
        });
    }

    private void signInAnonymous() {
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    }
                });
    }

    private void signInUser(String email, String password) {
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            Snackbar.make(binding.getRoot(), "Campos vacios", Snackbar.LENGTH_SHORT).show();
        } else {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Snackbar.make(binding.getRoot(), task.getException().getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void createUser(String email, String password) {
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            Snackbar.make(binding.getRoot(), "Campos vacios", Snackbar.LENGTH_SHORT).show();
        } else if (!binding.confirmPassword.getText().toString().equals(password)) {
            Snackbar.make(binding.getRoot(), "Las contraseñas no coinciden", Snackbar.LENGTH_SHORT).show();
        } else {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Snackbar.make(binding.getRoot(), task.getException().getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void onConnected(GoogleSignInAccount googleSignInAccount) {
        firebaseAuthWithGoogle(googleSignInAccount);
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        showProgressDialog("Iniciando sesión...");

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            updateUI(null);
                        }
                        hideProgressDialog();
                    }
                });
    }

    private void updateUI(final FirebaseUser user) {
        if (user != null) {
            startActivity(new Intent(context, MainActivity.class));
        } else {
            signInAnonymous();
        }
    }
}