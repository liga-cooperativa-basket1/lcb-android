package liga.cooperativa.basket.ligacooperativabasket.firebase.tables

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoFase

class TablesViewModel : ViewModel() {

    var groupSize = MutableLiveData(0)
    var myRef = FirebaseDatabase.getInstance().reference

    init {
        myRef.child("Fase").addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val fase = snapshot.getValue(TipoFase::class.java)
                groupSize.value = fase?.Grupos
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(error.details, error.message)
            }
        })
    }
}