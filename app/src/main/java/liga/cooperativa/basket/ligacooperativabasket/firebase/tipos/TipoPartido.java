package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("Serializable")
public class TipoPartido implements Serializable {

    public String Campo;
    public String Fecha;
    public String Local;
    public String Visitante;
    public String ResultadoLocal;
    public String ResultadoVisitante;
    public String Status;
    public String AutorID;
    public String DataBaseID;
    public String Temporada;
    public String Fase;
    public String ActualizadoPor;
    public String FechaActualizacion;
    public TipoValoracion Valoracion;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
    private SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE dd/MM", Locale.getDefault());
    private SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    public TipoPartido() {

    }

    public TipoPartido(Calendar calendar, String campo, String local, String visitante, String autorID, TipoFase fase) {
        this.Fecha = sdf.format(calendar.getTime());
        this.Campo = campo;
        this.Local = local;
        this.Visitante = visitante;
        this.ResultadoLocal = "SIN DEFINIR";
        this.ResultadoVisitante = "SIN DEFINIR";
        this.Status = "FIJADO";
        this.AutorID = autorID;
        this.DataBaseID = "SIN DEFINIR";
        this.Temporada = fase.Temporada;
        this.Fase = fase.Nombre;
        this.Valoracion = new TipoValoracion();
        this.Valoracion.Local = "SIN DEFINIR";
        this.Valoracion.Visitante = "SIN DEFINIR";
    }

    public static ArrayList<TipoPartido> obtenerPartidosEquipo(TipoEquipo e, ArrayList<TipoPartido> tipoPartidos) {
        ArrayList<TipoPartido> partidosEquipo = new ArrayList<>();
        for (TipoPartido p : tipoPartidos) {
            if (p.Local.equals(e.Nombre) || p.Visitante.equals(e.Nombre))
                partidosEquipo.add(p);
        }
        return partidosEquipo;
    }

    public static TipoPartido obtenerPartidoRivales(String equipo1, String equipo2, ArrayList<TipoPartido> partidos) {
        for (TipoPartido p : partidos) {
            if ((p.Local.equals(equipo1) || p.Visitante.equals(equipo1)) && (p.Local.equals(equipo2) || p.Visitante.equals(equipo2)))
                return p;
        }
        return null;
    }

    public static ArrayList<TipoPartido> obtenerPartidosJugados(ArrayList<ArrayList<TipoEquipo>> equipos, ArrayList<TipoPartido> partidos) {
        ArrayList<TipoPartido> partidosNoEliminables = new ArrayList<>();
        for (ArrayList<TipoEquipo> ae : equipos) {
            for (int i = 0; i < ae.size(); i++) {
                for (int j = i + 1; j < ae.size(); j++) {
                    TipoPartido p = TipoPartido.obtenerPartidoRivales(ae.get(i).Nombre, ae.get(j).Nombre, partidos);
                    if (p != null)
                        partidosNoEliminables.add(p);

                }
            }
        }
        return partidosNoEliminables;
    }

    public Date getMatchDate() {
        try {
            return sdf.parse(this.Fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String dateWithFormat() {
        return dayFormat.format(getMatchDate());
    }

    public String getInfo() {
        if (ResultadoLocal != null && !ResultadoLocal.equals("SIN DEFINIR") &&
                ResultadoVisitante != null && !ResultadoVisitante.equals("SIN DEFINIR")
        ) {
            return ResultadoLocal+" - "+ResultadoVisitante;
        } else {
            return hourFormat.format(getMatchDate());
        }
    }
}
