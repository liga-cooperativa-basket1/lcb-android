package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
//import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.listados.AdaptadorListas;

public class ElegirEquipo extends BaseActivity {

    private ArrayList<TipoEquipo> arrayListaTipoEquipos = new ArrayList<>();
    private ListView listaElegirEquipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_elegir_equipo);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(user.getEmail());
        textViewNombreUsuario.setText(user.getDisplayName());



        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                showProgressDialog("Actualizando equipos");

                arrayListaTipoEquipos = new ArrayList<>();

                if (!dataSnapshot.child("Equipos").exists())
                    Toast.makeText(context, "No existen equipos", Toast.LENGTH_LONG).show();
                else if (dataSnapshot.child("Equipos").getChildrenCount() > 0) {
                    iterable = dataSnapshot.child("Equipos").getChildren();


                    for (DataSnapshot ds : iterable) {
                        TipoEquipo e = ds.getValue(TipoEquipo.class);
                        if (e.Activo.equals("SI"))
                            arrayListaTipoEquipos.add(e);
                    }

                    Collections.sort(arrayListaTipoEquipos, new Comparator<TipoEquipo>() {
                        @Override
                        public int compare(TipoEquipo e2, TipoEquipo e1) {

                            return e1.Nombre.compareTo(e2.Nombre) * -1;
                        }
                    });

                    listaElegirEquipo = findViewById(R.id.listaElegirEquipo);
                    listaElegirEquipo.setAdapter(new AdaptadorListas(context, R.layout.elem_lista_elegirequipo, arrayListaTipoEquipos) {
                        @Override
                        public void onEntrada(Object entrada, View view) {
                            if (entrada != null) {
                                try {
                                    TipoEquipo tipoEquipo = (TipoEquipo) entrada;

                                    FirebaseStorage storage = FirebaseStorage.getInstance();
                                    StorageReference storageReference = storage.getReference().child(tipoEquipo.LinkLogo);

                                    ImageView logo_equipo = view.findViewById(R.id.logo_equipo);
                                    if (logo_equipo != null && storageReference != null)
                                        Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_equipo);

                                    TextView textViewPartido = view.findViewById(R.id.nombre_equipo);
                                    if (textViewPartido != null)
                                        textViewPartido.setText(tipoEquipo.Nombre);

                                } catch (Exception ex) {
                                    Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });

                    listaElegirEquipo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int posición, long l) {
                            TipoEquipo e = arrayListaTipoEquipos.get(posición);
                            myRef.child("Usuarios").child(mAuth.getUid()).child("Equipo").setValue(e.Nombre);

                            /*FirebaseInstallations.getInstance().getId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (!task.isSuccessful()) {
                                        return;
                                    }
                                    String token = task.getResult().getToken();
                                    myRef.child("Usuarios").child(mAuth.getUid()).child("TokenFCM").setValue(token);
                                    startActivity(new Intent(context, ObtenerDatosGoogle.class));
                                    finish();
                                }
                            });*/


                            FirebaseMessaging.getInstance().getToken()
                                    .addOnCompleteListener(new OnCompleteListener<String>() {
                                        @Override
                                        public void onComplete(@NonNull Task<String> task) {
                                            if (!task.isSuccessful()) {
                                                Log.w("", "Fetching FCM registration token failed", task.getException());
                                                return;
                                            }

                                            // Get new FCM registration token
                                            String token = task.getResult();

                                            myRef.child("Usuarios").child(mAuth.getUid()).child("TokenFCM").setValue(token);
                                            startActivity(new Intent(context, ObtenerDatosGoogle.class));
                                            finish();

                                            // Log and toast
                                            /*String msg = getString(R.string.msg_token_fmt, token);
                                            Log.d("", msg);*/
                                            //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                                        }
                                    });

                            startActivity(new Intent(context, MainActivity.class));
                        }
                    });
                }

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
