package liga.cooperativa.basket.ligacooperativabasket.firebase;

import static liga.cooperativa.basket.ligacooperativabasket.firebase.utils.TypeExtensionsKt.getTeamLogo;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import org.jetbrains.annotations.NotNull;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import liga.cooperativa.basket.ligacooperativabasket.BR;
import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.databinding.ItemGameBinding;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoCancha;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoUsuario;


public class MostrarEquipo extends BaseActivity {

    private TipoEquipo equipo;
    private ArrayList<TipoPartido> partidos;
    private ArrayList<TipoEquipo> equipos;
    private ArrayList<TipoUsuario> usuarios;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.content_mostrar_equipo);
            MaterialToolbar toolbar = findViewById(R.id.topAppBar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            mAuth = FirebaseAuth.getInstance();
            final FirebaseUser user = mAuth.getCurrentUser();

            myRef = FirebaseDatabase.getInstance().getReference();

            equipo = (TipoEquipo) getIntent().getExtras().getSerializable("Equipo");
            if (equipo.Activo.equals("NO")) {
                ImageView rl = findViewById(R.id.layout_desactivado);
                rl.setVisibility(View.VISIBLE);
            }

            ImageView logo_equipo = findViewById(R.id.logo_equipo);

            storage = FirebaseStorage.getInstance();
            storageReference = storage.getReference().child(equipo.LinkLogo);
            Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_equipo);


            TextView victorias = findViewById(R.id.victorias);
            TextView derrotas = findViewById(R.id.derrotas);
            TextView porcentaje = findViewById(R.id.porcentaje);
            TextView diferencia = findViewById(R.id.diferencia);

            toolbar.setTitle(equipo.Nombre);
            victorias.setText("" + equipo.Clasificacion.Victorias);
            derrotas.setText("" + equipo.Clasificacion.Derrotas);

            double porcentajeVictorias = 0;
            if (equipo.Clasificacion.Victorias + equipo.Clasificacion.Derrotas != 0)
                porcentajeVictorias = (double) equipo.Clasificacion.Victorias / (equipo.Clasificacion.Victorias + equipo.Clasificacion.Derrotas) * 100;

            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            porcentaje.setText(df.format(porcentajeVictorias) + "%");
            int diferenciaCanastas = (equipo.Clasificacion.CanastasFavor - equipo.Clasificacion.CanastasContra);
            if (diferenciaCanastas > 0)
                diferencia.setText("+" + diferenciaCanastas);
            else
                diferencia.setText("" + diferenciaCanastas);


            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot dataSnapshot) {

                    try {
                        partidos = new ArrayList<>();
                        equipos = new ArrayList<>();


//                        textoVotacion = findViewById(R.id.textoVotacion);
//
//                        actualizaVotaciones(dataSnapshot);
//
//                        if (!equipo.Nombre.equals(miUsuario.Equipo)) {
//                            llenaEstrellasAmarillas(miVotacion);
//                            star1.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//
//                                    TipoVotacion.actualizaVotacion(equipo.Nombre, miUsuario, 1);
//                                    llenaEstrellasAmarillas(1);
//                                    myRef.child("Usuarios").child(mAuth.getUid()).setValue(miUsuario);
////                            actualizaVotaciones(dataSnapshot);
//                                }
//                            });
//
//                            star2.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    try {
//                                        TipoVotacion.actualizaVotacion(equipo.Nombre, miUsuario, 2);
//                                        llenaEstrellasAmarillas(2);
//                                        myRef.child("Usuarios").child(mAuth.getUid()).setValue(miUsuario);
//                                        actualizaVotaciones(dataSnapshot);
//                                    } catch (Exception ex) {
//                                        int a = 2;
//                                    }
//                                }
//                            });
//
//                            star3.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    TipoVotacion.actualizaVotacion(equipo.Nombre, miUsuario, 3);
//                                    llenaEstrellasAmarillas(3);
//                                    myRef.child("Usuarios").child(mAuth.getUid()).setValue(miUsuario);
////                            actualizaVotaciones(dataSnapshot);
//                                }
//                            });
//
//                            star4.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    TipoVotacion.actualizaVotacion(equipo.Nombre, miUsuario, 4);
//                                    llenaEstrellasAmarillas(4);
//                                    myRef.child("Usuarios").child(mAuth.getUid()).setValue(miUsuario);
////                            actualizaVotaciones(dataSnapshot);
//                                }
//                            });
//
//                            star5.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    TipoVotacion.actualizaVotacion(equipo.Nombre, miUsuario, 5);
//                                    llenaEstrellasAmarillas(5);
//                                    myRef.child("Usuarios").child(mAuth.getUid()).setValue(miUsuario);
////                            actualizaVotaciones(dataSnapshot);
//                                }
//                            });
//                        } else
//                            llenaEstrellasNegras(mediaVotos);
//                        Toast.makeText(context, "Votación actualizada", Toast.LENGTH_LONG).show();

//                if (!dataSnapshot.child("Partidos").exists())
//                    return;

                        iterable = dataSnapshot.child("Partidos").getChildren();
                        for (DataSnapshot ds : iterable) {
                            TipoPartido tp = ds.getValue(TipoPartido.class);
                            if ((tp.Local.equals(equipo.Nombre) || tp.Visitante.equals(equipo.Nombre)) && tp.Status.equals("JUGADO"))
                                partidos.add(tp);
                        }

                        iterable = dataSnapshot.child("Equipos").getChildren();
                        for (DataSnapshot ds : iterable) {
                            TipoEquipo te = ds.getValue(TipoEquipo.class);
                            equipos.add(te);
                        }


                        String canchaMasUsada = TipoCancha.obtenerCanchaMasUsada(partidos);

                        TextView cancha_mas_usada = findViewById(R.id.cancha_mas_usada);

                        if (canchaMasUsada != null)
                            cancha_mas_usada.setText(canchaMasUsada);
                        else
                            cancha_mas_usada.setText("SIN DEFINIR");

                        new LastAdapter(partidos, BR.game)
                                .map(String.class, R.layout.item_date)
                                .map(TipoPartido.class, new ItemType<ItemGameBinding>(R.layout.item_game) {
                                    @Override
                                    public void onBind(@NotNull Holder<ItemGameBinding> holder) {
                                        TipoPartido game = partidos.get(holder.getBindingAdapterPosition());
                                        holder.getBinding().setLogoHome(getTeamLogo(dataSnapshot, storage, game.Local));
                                        holder.getBinding().setLogoAway(getTeamLogo(dataSnapshot, storage, game.Visitante));
                                    }
                                })
                                .into(findViewById(R.id.recyclerView));

                        hideProgressDialog();
                    } catch (Exception ex) {
                        int a = 2;
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            hideProgressDialog();
        } catch (Exception ex) {
            int a = 3;
        }
    }

//    public void actualizaVotaciones(DataSnapshot dataSnapshot) {
//        iterable = dataSnapshot.child("Usuarios").getChildren();
//        usuarios = new ArrayList<>();
//        for (DataSnapshot ds : iterable) {
//            TipoUsuario usuario = ds.getValue(TipoUsuario.class);
//            if (ds.getKey().equals(mAuth.getUid()))
//                if (miUsuario != null)
//                    usuarios.add(miUsuario);
//                else {
//                    usuarios.add(usuario);
//                    miUsuario = usuario;
//                }
//            else
//                usuarios.add(usuario);
//        }
//
//        mediaVotos = TipoVotacion.obtenerVotacionesEquipo(equipo.Nombre, usuarios);
//        numeroVotos = TipoVotacion.obtenerNumeroVotosEquipo(equipo.Nombre, usuarios);
//        miVotacion = TipoVotacion.miVotacion(equipo.Nombre, miUsuario);
//        textoVotacion.setText("Votación equipo: " + mediaVotos + " estrellas (" + numeroVotos + " votos)");
//    }

    private boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        if (equipo.Activo.equals("NO")) {
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }
        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                iterable = dataSnapshot.child("Administradores").getChildren();

                for (DataSnapshot ds : iterable) {
                    if (ds.child("Email").getValue().equals(mAuth.getCurrentUser().getEmail())) {
                        getMenuInflater().inflate(R.menu.main_administradores, menu);
                        return;
                    }
                }
                getMenuInflater().inflate(R.menu.main, menu);
                return;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_desactivar) {
            final ArrayList<TipoPartido> partidosEquipo = new ArrayList<>();
            final ArrayList<TipoEquipo> equiposGrupo = new ArrayList<>();
            myRef = FirebaseDatabase.getInstance().getReference();

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    iterable = dataSnapshot.child("Partidos").getChildren();

                    for (DataSnapshot ds : iterable) {
                        TipoPartido p = ds.getValue(TipoPartido.class);
                        p.DataBaseID = ds.getKey();
                        if (p.Local.equals(equipo.Nombre) || p.Visitante.equals(equipo.Nombre))
                            partidosEquipo.add(p);
                    }

                    iterable = dataSnapshot.child("Equipos").getChildren();
                    for (DataSnapshot ds : iterable) {
                        TipoEquipo e = ds.getValue(TipoEquipo.class);
                        e.DataBaseID = ds.getKey();
                        if (e.Grupo == equipo.Grupo && !e.Nombre.equals(equipo.Nombre))
                            equiposGrupo.add(e);

                    }

                    /*Equipos con partido jugado o fijado*/
                    for (TipoPartido p : partidosEquipo) {
                        TipoEquipo local = TipoEquipo.obtenerEquipoPorNombre(p.Local, equiposGrupo);
                        TipoEquipo visitante = TipoEquipo.obtenerEquipoPorNombre(p.Visitante, equiposGrupo);
                        equiposGrupo.remove(local);
                        equiposGrupo.remove(visitante);

                        if (p.Status.equals("FIJADO") || p.Status.equals("DOBLE VERIFICACION")) {
                            p.Status = "JUGADO";
                            if (local.Nombre.equals(equipo.Nombre)) {
                                p.ResultadoLocal = "0";
                                p.ResultadoVisitante = "20";
                                visitante.Clasificacion.Victorias++;
                                visitante.Clasificacion.CanastasFavor += 20;
                                myRef.child("Equipos").child(visitante.DataBaseID).setValue(visitante);
                            } else {
                                p.ResultadoVisitante = "0";
                                p.ResultadoLocal = "20";
                                local.Clasificacion.Victorias++;
                                local.Clasificacion.CanastasFavor += 20;
                                myRef.child("Equipos").child(local.DataBaseID).setValue(local);
                            }
                            myRef.child("Partidos").child(p.DataBaseID).setValue(p);
                        } else {
                            if (local.Nombre.equals(equipo.Nombre)) {
                                if (Integer.parseInt(p.ResultadoLocal) > Integer.parseInt(p.ResultadoVisitante)) {
                                    visitante.Clasificacion.Derrotas--;
                                    visitante.Clasificacion.Victorias++;
                                }
                                visitante.Clasificacion.CanastasFavor -= Integer.parseInt(p.ResultadoVisitante);
                                visitante.Clasificacion.CanastasFavor += 20;
                                visitante.Clasificacion.CanastasContra -= Integer.parseInt(p.ResultadoLocal);
                                myRef.child("Equipos").child(visitante.DataBaseID).setValue(visitante);

                            } else {
                                if (Integer.parseInt(p.ResultadoLocal) < Integer.parseInt(p.ResultadoVisitante)) {
                                    local.Clasificacion.Derrotas--;
                                    local.Clasificacion.Victorias++;
                                }
                                local.Clasificacion.CanastasFavor -= Integer.parseInt(p.ResultadoLocal);
                                local.Clasificacion.CanastasFavor += 20;
                                local.Clasificacion.CanastasContra -= Integer.parseInt(p.ResultadoVisitante);
                                myRef.child("Equipos").child(local.DataBaseID).setValue(local);
                            }
                            myRef.child("Partidos").child(p.DataBaseID).setValue(p);
                        }
                    }


                    /*Equipos sin jugar partido*/
                    for (TipoEquipo e : equiposGrupo) {
                        TipoPartido p = new TipoPartido();
                        p.Local = equipo.Nombre;
                        p.Visitante = e.Nombre;
                        p.ResultadoVisitante = "20";
                        p.ResultadoLocal = "0";
                        p.Status = "JUGADO";
                        p.Campo = "SIN DEFINIR";
                        p.AutorID = mAuth.getUid();
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        p.Fecha = sdf.format(c.getTime());
                        myRef.child("Partidos").push().setValue(p);
                        e.Clasificacion.Victorias++;
                        e.Clasificacion.CanastasFavor += 20;
                        myRef.child("Equipos").child(e.DataBaseID).setValue(e);

                    }


                    ImageView rl = findViewById(R.id.layout_desactivado);
                    rl.setVisibility(View.VISIBLE);

                    equipo.Activo = "NO";
                    myRef.child("Equipos").child(equipo.DataBaseID).setValue(equipo);


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

//    public void llenaEstrellasNegras(double valor) {
//        star1 = findViewById(R.id.star1);
//        star2 = findViewById(R.id.star2);
//        star3 = findViewById(R.id.star3);
//        star4 = findViewById(R.id.star4);
//        star5 = findViewById(R.id.star5);
//
//        if (valor == 0) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor > 0 && valor < 1) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_half));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 1) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor > 1 && valor < 2) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_half));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 2) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor > 2 && valor < 3) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_half));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 3) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor > 3 && valor < 4) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_half));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 4) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor > 4 && valor < 5) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_half));
//        } else if (valor == 5) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_black));
//        }
//    }
//
//    public void llenaEstrellasAmarillas(int valor) {
//        star1 = findViewById(R.id.star1);
//        star2 = findViewById(R.id.star2);
//        star3 = findViewById(R.id.star3);
//        star4 = findViewById(R.id.star4);
//        star5 = findViewById(R.id.star5);
//
//        if (valor == 0) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 1) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 2) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 3) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 4) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_border));
//        } else if (valor == 5) {
//            star1.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star2.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star3.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star4.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//            star5.setImageDrawable(getResources().getDrawable(R.drawable.star_yellow));
//        }
//    }

}
