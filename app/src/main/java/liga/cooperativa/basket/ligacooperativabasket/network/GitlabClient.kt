package liga.cooperativa.basket.ligacooperativabasket.network

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

object GitlabClient {

    private const val baseUrl = "https://gitlab.com/api/v4/projects/31651353/"
    const val repoUrl = "https://gitlab.com/liga-cooperativa-basket1/lcb-android"

    fun getInstance(): Retrofit {
        return Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }
}

interface GitlabService {
    @GET("releases")
    suspend fun getReleases(): List<GitlabRelease>
}
