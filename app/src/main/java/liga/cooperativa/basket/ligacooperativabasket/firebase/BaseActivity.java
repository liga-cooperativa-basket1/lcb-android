package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoAdministradores;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

//    @VisibleForTesting
    public ProgressDialog mProgressDialog;
    public Context context = this;
    public DatabaseReference myRef;
    public FirebaseAuth mAuth;
    public Iterable<DataSnapshot> iterable;
    public String miEquipoSt;
    public TipoEquipo miEquipo;
    FirebaseStorage storage;
    StorageReference storageReference;

    public void showProgressDialog(String mensaje) {
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(mensaje);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        String prueba ="prueba";
        int id = item.getItemId();
        myRef = FirebaseDatabase.getInstance().getReference();

        if (id == R.id.menu_menuPrincipal) {
            startActivity(new Intent(context, MainActivity.class));
            finish();
        } else if (id == R.id.menu_noticias) {
            startActivity(new Intent(context, Noticias.class));
        } else if (id == R.id.menu_miEquipo) {
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    miEquipoSt = (String) dataSnapshot.child("Usuarios").child(mAuth.getUid()).child("Equipo").getValue();

                    iterable = dataSnapshot.child("Equipos").getChildren();

                    for (DataSnapshot ds : iterable) {
                        TipoEquipo te = ds.getValue(TipoEquipo.class);
                        if (te.Nombre.equals(miEquipoSt)) {
                            miEquipo = te;
                            break;
                        }
                    }
                    Intent intent = new Intent(context, MostrarEquipo.class);
                    intent.putExtra("Equipo", miEquipo);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (id == R.id.menu_fijarPartido) {
            startActivity(new Intent(context, FijarPartidoRival.class));
        } else if (id == R.id.menu_canchas) {
            startActivity(new Intent(context, Canchas.class));
        } else if (id == R.id.administradores) {

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    iterable = dataSnapshot.child("Administradores").getChildren();

                    for (DataSnapshot ds : iterable) {
                        TipoAdministradores administrador = ds.getValue(TipoAdministradores.class);
                        if (administrador.Email.equals(mAuth.getCurrentUser().getEmail())) {
                            startActivity(new Intent(context, IniciarLiga.class));
                            finish();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (id == R.id.menu_logout) {
            mAuth.signOut();
            finish();
            startActivity(new Intent(context, LoginActivity.class));
        } else if (id == R.id.about) {
            startActivity(new Intent(context, InfoActivity.class));
        }

//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }


}

