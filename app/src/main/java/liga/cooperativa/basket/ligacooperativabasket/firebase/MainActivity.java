package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.databinding.LayoutMainActivityBinding;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tables.TablesFragment;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoAdministradores;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;

public class MainActivity extends BaseActivity {

//    FloatingActionButton fab;
    LayoutMainActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_main_activity);

        /*fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, IniciarLiga.class));
                finish();
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });*/

        binding.topAppBar.setNavigationOnClickListener(v -> binding.drawerLayout.open());

        binding.navView.setNavigationItemSelectedListener(item -> {
            // Handle menu item selected
            item.setChecked(true);
            binding.drawerLayout.close();
            return true;
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnItemSelectedListener(item -> {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.menu_calendario:
                    selectedFragment = new Calendario();
                    break;
                case R.id.menu_clasificacion:
                    selectedFragment = TablesFragment.newInstance();
                    break;
                case R.id.menu_resultados:
                    selectedFragment = new Resultados();
                    break;
            }
            if (selectedFragment != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, selectedFragment)
                        .commit();
            }
            return true;
        });
        bottomNavigationView.setSelectedItemId(R.id.menu_calendario);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Ocultar hasta que funcione
        navigationView.getMenu().findItem(R.id.menu_miEquipo).setVisible(false);

        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(mAuth.getCurrentUser().getEmail());
        myRef.child("Usuarios").child(mAuth.getUid()).child("Equipo").get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult().getValue() != null) {
                textViewNombreUsuario.setText(String.valueOf(task.getResult().getValue()));
            } else {
                textViewNombreUsuario.setText("Liga Cooperativa Basket");
                navigationView.getMenu().findItem(R.id.menu_fijarPartido).setVisible(false);
                bottomNavigationView.getMenu().findItem(R.id.menu_resultados).setVisible(false);
            }

        });


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String fechaFin = (String) dataSnapshot.child("Fase").child("FIN").getValue();
                String fechaInicio = (String) dataSnapshot.child("Fase").child("INICIO").getValue();

                Boolean administrador = false;

                iterable = dataSnapshot.child("Administradores").getChildren();
                for (DataSnapshot ds : iterable) {
                    TipoAdministradores admin = ds.getValue(TipoAdministradores.class);
                    if (admin.Email.equals(mAuth.getCurrentUser().getEmail()))
                        administrador = true;
                }

                navigationView.getMenu().findItem(R.id.administradores).setVisible(administrador);

                /*iterable = dataSnapshot.child("Equipos").getChildren();
                ArrayList<TipoEquipo> equipos = new ArrayList<>();
                for (DataSnapshot ds : iterable) {
                    TipoEquipo equipo = ds.getValue(TipoEquipo.class);
                    equipos.add(equipo);
                }

                iterable = dataSnapshot.child("Canchas").getChildren();
                ArrayList<TipoCancha> canchas = new ArrayList<>();
                for (DataSnapshot ds : iterable) {
                    TipoCancha cancha = ds.getValue(TipoCancha.class);
                    canchas.add(cancha);
                }

                myRef.child("Equipos").removeValue();
                for (TipoEquipo e: equipos) {
                    myRef.child("Equipos").push().setValue(e);
                }

                myRef.child("Canchas").removeValue();
                for(TipoCancha c : canchas)
                {
                    myRef.child("Canchas").push().setValue(c);
                }*/



//                if (!administrador)
//                    fab.setVisibility(View.GONE);


                if (fechaFin != null && fechaInicio != null) {
                    Calendar cActual = Calendar.getInstance();
                    Calendar cFinDeFase = Calendar.getInstance();
                    Calendar cInicioDeFase = Calendar.getInstance();

                    cFinDeFase.set(Integer.parseInt(fechaFin.split("/")[2]), Integer.parseInt(fechaFin.split("/")[1]) - 1, Integer.parseInt(fechaFin.split("/")[0]));
                    cInicioDeFase.set(Integer.parseInt(fechaInicio.split("/")[2]), Integer.parseInt(fechaInicio.split("/")[1]) - 1, Integer.parseInt(fechaInicio.split("/")[0]));

//                    if (cActual.after(cFinDeFase))
//                        myRef.child("Fase").removeValue();
//                    else if (cActual.before(cFinDeFase) && cActual.after(cInicioDeFase))
//                        fab.setVisibility(View.GONE);
                }
                miEquipoSt = (String) dataSnapshot.child("Usuarios").child(mAuth.getUid()).child("Equipo").getValue();


                iterable = dataSnapshot.child("Partidos").getChildren();


                for (DataSnapshot ds : iterable) {
                    TipoPartido tp = ds.getValue(TipoPartido.class);
                    if (!(tp.Local.equals(miEquipoSt) || tp.Visitante.equals(miEquipoSt)))
                        continue;

                    String fechaSt = tp.Fecha;

                    Calendar fechaPartido = Calendar.getInstance();
                    fechaPartido.set(Integer.parseInt(fechaSt.split("/")[2].split(" ")[0]), Integer.parseInt(fechaSt.split("/")[1]) - 1, Integer.parseInt(fechaSt.split("/")[0]), Integer.parseInt(fechaSt.split(" ")[1].split(":")[0]), Integer.parseInt(fechaSt.split(" ")[1].split(":")[1]));
                    fechaPartido.add(Calendar.MINUTE, 75);
                    Calendar fechaActual = Calendar.getInstance();


                    if (fechaPartido.before(fechaActual) && !tp.Status.equals("JUGADO")) {
                        View view = findViewById(android.R.id.content);
                        Snackbar.make(view, "Tienes partidos pendientes de introducir un resultado", Snackbar.LENGTH_LONG).setAction("IR", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                bottomNavigationView.setSelectedItemId(R.id.menu_resultados);
                            }
                        }).show();
                        break;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("¿Seguro que desea salir?")
                .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        builder.create();
        builder.show();
    }
}
