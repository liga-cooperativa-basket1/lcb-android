package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("Serializable")
public class TipoEquipo implements Serializable {
    public String Nombre;
    public String LinkImagen;
    public String LinkLogo;
    public String DataBaseID;
    public String Activo;
    public String DesactivadoPor;
    public int Grupo;
    public TipoClasificacion Clasificacion;

    public TipoEquipo() {

    }

    public TipoEquipo(String name) {
        this.Nombre= name;
        this.LinkLogo = "logo";
        this.Activo = "SI";
    }

    public static TipoEquipo obtenerEquipoPorNombre(String nombre, ArrayList<TipoEquipo> tipoEquipos) {
        for (TipoEquipo e : tipoEquipos) {
            if (e.Nombre.equals(nombre))
                return e;
        }
        return null;
    }

    public static ArrayList<TipoEquipo> configuraGruposInicioLiga(ArrayList<TipoEquipo> equipos, int numeroDeGrupos) {
        ArrayList<TipoEquipo> equiposConGrupo = new ArrayList<>();
        Random r = new Random();
        int grupo = 0;

        while (equipos.size() != 0) {
            int elegido = r.nextInt();
            if (elegido < 0)
                elegido *= -1;
            elegido %= equipos.size();

            TipoEquipo e = equipos.get(elegido);
            e.Grupo = ((grupo % numeroDeGrupos) + 1);
            grupo++;
            equiposConGrupo.add(equipos.get(elegido));
            equipos.remove(elegido);
        }


        return equiposConGrupo;
    }

    public static ArrayList<TipoEquipo> obtenerEquiposGrupo(TipoEquipo e, ArrayList<TipoEquipo> tipoEquipos) {
        ArrayList<TipoEquipo> equiposGrupo = new ArrayList<>();
        for (TipoEquipo eq : tipoEquipos) {
            if (e.Grupo == eq.Grupo && e != eq)
                equiposGrupo.add(eq);
        }
        return equiposGrupo;
    }

    public static ArrayList<TipoEquipo> obtenerEquiposPorFijar(ArrayList<TipoEquipo> tipoEquipos, ArrayList<TipoPartido> tipoPartidos) {
        for (TipoPartido p : tipoPartidos) {
            TipoEquipo el = TipoEquipo.obtenerEquipoPorNombre(p.Local, tipoEquipos);
            if (el != null)
                tipoEquipos.remove(el);

            TipoEquipo ev = TipoEquipo.obtenerEquipoPorNombre(p.Visitante, tipoEquipos);
            if (ev != null)
                tipoEquipos.remove(ev);
        }
        return tipoEquipos;
    }

    public static ArrayList<ArrayList<TipoEquipo>> obtenerEquiposGrupos(ArrayList<TipoEquipo> equipos, int numGrupos) {
        ArrayList<ArrayList<TipoEquipo>> grupos = new ArrayList<>();
        for (int i = 0; i < numGrupos; i++)
            grupos.add(new ArrayList<TipoEquipo>());

        for (TipoEquipo e : equipos)
            grupos.get(e.Grupo - 1).add(e);

        return grupos;
    }


}
