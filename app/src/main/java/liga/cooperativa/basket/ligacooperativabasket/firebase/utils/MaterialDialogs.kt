package liga.cooperativa.basket.ligacooperativabasket.firebase.utils

import android.annotation.SuppressLint
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.dateTimePicker
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import liga.cooperativa.basket.ligacooperativabasket.R
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.GameOptionItem
import java.util.Calendar

fun MaterialDialog.showDateTime(onDateSelected: (Calendar) -> Unit) {
    show {
        dateTimePicker(
            autoFlipToTime = true,
            requireFutureDateTime = true,
            show24HoursView = true
        ) { dialog, datetime ->
            onDateSelected.invoke(datetime)
            dialog.dismiss()
        }
    }
}

@SuppressLint("CheckResult")
fun MaterialDialog.showInput(title: String, onInputValue: (String) -> Unit) {
    show {
        title(text = title)
        input { materialDialog, charSequence ->
            onInputValue.invoke(charSequence.toString())
            materialDialog.dismiss()
        }
        positiveButton(R.string.submit)
    }
}

@SuppressLint("CheckResult")
fun MaterialDialog.gameOptionBottomSheet(items: List<GameOptionItem>) {
    show {
        listItems(items = items.map { it.title }) { _, index, _ ->
            items[index].callback.invoke()
        }
    }
}