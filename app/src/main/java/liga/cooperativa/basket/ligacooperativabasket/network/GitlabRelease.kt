package liga.cooperativa.basket.ligacooperativabasket.network

data class GitlabRelease(
    var name: String,
    var tag_name: String,
    var description: String,
    var released_at: String
)
