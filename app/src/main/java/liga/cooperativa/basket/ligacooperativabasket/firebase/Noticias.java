package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;



import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoAdministradores;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoNoticia;
import liga.cooperativa.basket.ligacooperativabasket.listados.AdaptadorListas;

public class Noticias extends BaseActivity {

    TipoNoticia tipoNoticiaSeleccionado;
    private ArrayList<TipoNoticia> arrayListTipoNoticia = new ArrayList<>();
    private ListView listViewNoticias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_noticias);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();

        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                showProgressDialog("Obteniendo noticias...");
                arrayListTipoNoticia = new ArrayList<>();

                listViewNoticias = findViewById(R.id.listaNoticias);

                final Iterable<DataSnapshot> iterableAdministradores = dataSnapshot.child("Administradores").getChildren();
                final ArrayList<TipoAdministradores> administradores = new ArrayList<>();
                for (DataSnapshot ds : iterableAdministradores) {
                    administradores.add(ds.getValue(TipoAdministradores.class));
                }

                FloatingActionButton fab = findViewById(R.id.fab);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean flag = false;
                        for (TipoAdministradores administrador : administradores) {
                            if (administrador.Email.equals(mAuth.getCurrentUser().getEmail())) {
                                flag = true;
                                startActivity(new Intent(context, CrearNoticia.class));
                                finish();
                            }
                        }
                        if (!flag) {
                            Toast.makeText(context, "No tienes permisos para realizar esta acción", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                if (!dataSnapshot.child("Noticias").exists()) {
                    if (arrayListTipoNoticia.isEmpty()) {
                        listViewNoticias.setVisibility(View.GONE);
                        TextView textoSinNoticias = findViewById(R.id.textoSinNoticias);
                        textoSinNoticias.setVisibility(View.VISIBLE);
                    }
                } else if (dataSnapshot.child("Noticias").getChildrenCount() > 0) {
                    iterable = dataSnapshot.child("Noticias").getChildren();

                    for (DataSnapshot ds : iterable) {
                        TipoNoticia tn = ds.getValue(TipoNoticia.class);
                        tn.DatabaseId = ds.getKey();
                        arrayListTipoNoticia.add(tn);
                    }

                    if (arrayListTipoNoticia.isEmpty()) {
                        listViewNoticias.setVisibility(View.GONE);
                        TextView textoSinNoticias = findViewById(R.id.textoSinNoticias);
                        textoSinNoticias.setVisibility(View.VISIBLE);
                    }


                    listViewNoticias.setAdapter(new AdaptadorListas(context, R.layout.elem_lista_noticias, arrayListTipoNoticia) {
                        @Override
                        public void onEntrada(Object entrada, View view) {
                            if (entrada != null) {
                                try {
                                    TipoNoticia tipoNoticia = (TipoNoticia) entrada;

                                    TextView titulo = view.findViewById(R.id.titulo);
                                    if (titulo != null)
                                        titulo.setText(tipoNoticia.Titulo);

                                    TextView autor = view.findViewById(R.id.autor);
                                    if (autor != null)
                                        autor.setText("\tAutor: " + tipoNoticia.Autor);

                                    TextView fecha = view.findViewById(R.id.fecha);
                                    if (fecha != null)
                                        fecha.setText("\tFecha :" + tipoNoticia.Fecha);

                                    TextView cuerpo = view.findViewById(R.id.cuerpo);
                                    if (cuerpo != null)
                                        cuerpo.setText(tipoNoticia.Cuerpo);

                                } catch (Exception ex) {
                                    Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });

                    listViewNoticias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int posición, long l) {
                            tipoNoticiaSeleccionado = arrayListTipoNoticia.get(posición);
                            Intent intent = new Intent(context, MostrarNoticia.class);
                            intent.putExtra("Noticia", tipoNoticiaSeleccionado);
                            startActivity(intent);

                        }
                    });
                    registerForContextMenu(listViewNoticias);


                }

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.listaCalendario) {
            tipoNoticiaSeleccionado = (TipoNoticia) ((ListView) v).getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_lista_noticias, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editar_noticia:
                Intent intent = new Intent(context, CrearNoticia.class);
                intent.putExtra("Noticia", tipoNoticiaSeleccionado);
                startActivity(intent);
                finish();
                return true;

            case R.id.borrar_noticia:
                if (tipoNoticiaSeleccionado.AutorID.equals(mAuth.getUid())) {
                    myRef.child("Noticias").child(tipoNoticiaSeleccionado.DatabaseId).removeValue();
                    Toast.makeText(context, "Noticia borrada correctamente", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(context, Noticias.class));
                    finish();
                } else
                    Toast.makeText(context, "No ha sido posible borrar la noticia", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }
}
