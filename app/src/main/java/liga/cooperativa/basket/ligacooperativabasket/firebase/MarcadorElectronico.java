package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;


import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;


import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoClasificacion;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoRetransmision;

public class MarcadorElectronico extends BaseActivity {

    private final Context context = this;
    FloatingActionButton fab;
    TextView txtViewNombreLocal;
    Button buttonMenosUnoLocal;
    Button buttonMenosDosLocal;
    Button buttonMenosTresLocal;
    TextView textViewMarcadorLocal;
    Button buttonMasUnoLocal;
    Button buttonMasDosLocal;
    Button buttonMasTresLocal;
    Button buttonMenosFaltaLocal;
    RadioButton radioButtonFaltaUnoLocal;
    RadioButton radioButtonFaltaDosLocal;
    RadioButton radioButtonFaltaTresLocal;
    RadioButton radioButtonFaltaCuatroLocal;
    RadioButton radioButtonFaltaCincoLocal;
    Button buttonMasFaltaLocal;
    Button buttonStopTiempo;
    TextView textViewTiempoPartido;
    Button buttonStartPauseTiempo;
    TextView textViewCuartoPartido;
    TextView txtViewNombreVisitante;
    Button buttonMenosUnoVisitante;
    Button buttonMenosDosVisitante;
    Button buttonMenosTresVisitante;
    TextView textViewMarcadorVisitante;
    Button buttonMasUnoVisitante;
    Button buttonMasDosVisitante;
    Button buttonMasTresVisitante;
    Button buttonMenosFaltaVisitante;
    RadioButton radioButtonFaltaUnoVisitante;
    RadioButton radioButtonFaltaDosVisitante;
    RadioButton radioButtonFaltaTresVisitante;
    RadioButton radioButtonFaltaCuatroVisitante;
    RadioButton radioButtonFaltaCincoVisitante;
    Button buttonMasFaltaVisitante;
    CountDownTimer countDownTimer;
    boolean tiempoCorriendo;
    long tiempoRestante;
    int marcadorLocal;
    int faltasLocal;
    int marcadorVisitante;
    int faltasVisitante;
    int cuarto;
    int numeroPartido;

    TipoPartido partido;
    TipoEquipo equipoLocal, equipoVisitante;
    TipoRetransmision retransmision;


    /*@Override
    protected void onRestart() {
        if (myRef != null && partido != null) {
            myRef.child("Partidos").child(partido.DataBaseID).child("Status").setValue("JUGANDO");
        }
        super.onRestart();
    }*/

   /* @Override
    public void onStop() {
        if (myRef != null && partido != null) {
            myRef.child("Partidos").child(partido.DataBaseID).child("Status").setValue("FIJADO");
            pararContador(countDownTimer);
            retransmision.Tiempo = "FINALIZADO";
            myRef.child("Retransmisiones").child(retransmision.DatabaseID).setValue(retransmision);
        }
        super.onStop();
    }*/

    @Override
    public void onDestroy() {
        if (myRef != null && partido != null) {
            myRef.child("Partidos").child(partido.DataBaseID).child("Status").setValue("FIJADO");
            pararContador(countDownTimer);
            retransmision.Tiempo = "FINALIZADO";
            myRef.child("Retransmisiones").child(retransmision.DatabaseID).setValue(retransmision);
        }
        super.onDestroy();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_marcador_electronico);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /*fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("¿Guardar el resultado " + partido.Local + " " + marcadorLocal + " - " + partido.Visitante + " " + marcadorVisitante + "?")
                        .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                partido.Status = "JUGADO";
                                partido.ResultadoLocal = "" + marcadorLocal;
                                partido.ResultadoVisitante = "" + marcadorVisitante;

                                myRef.child("Partidos").child(partido.DataBaseID).setValue(partido);

                                if (Integer.parseInt(partido.ResultadoLocal) > Integer.parseInt(partido.ResultadoVisitante)) {
                                    equipoLocal.Clasificacion.Victorias++;
                                    equipoVisitante.Clasificacion.Derrotas++;
                                } else {
                                    equipoLocal.Clasificacion.Derrotas++;
                                    equipoVisitante.Clasificacion.Victorias++;
                                }

                                equipoLocal.Clasificacion.CanastasFavor += Integer.parseInt(partido.ResultadoLocal);
                                equipoLocal.Clasificacion.CanastasContra += Integer.parseInt(partido.ResultadoVisitante);
                                equipoVisitante.Clasificacion.CanastasFavor += Integer.parseInt(partido.ResultadoVisitante);
                                equipoVisitante.Clasificacion.CanastasContra += Integer.parseInt(partido.ResultadoLocal);

                                myRef.child("Equipos").child(equipoLocal.DataBaseID).setValue(equipoLocal);
                                myRef.child("Equipos").child(equipoVisitante.DataBaseID).setValue(equipoVisitante);


                                myRef = null;
                                partido = null;
                                Intent i = new Intent(context, Valoracion.class);
                                i.putExtra("Partido", partido);
                                startActivity(i);
                                finish();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(context, "Revise el resultado y pulse el boton inferior", Toast.LENGTH_LONG).show();

                                buttonMasUnoLocal.setEnabled(true);
                                buttonMasDosLocal.setEnabled(true);
                                buttonMasTresLocal.setEnabled(true);
                                buttonMenosUnoLocal.setEnabled(true);
                                buttonMenosDosLocal.setEnabled(true);
                                buttonMenosTresLocal.setEnabled(true);

                                buttonMasUnoVisitante.setEnabled(true);
                                buttonMasDosVisitante.setEnabled(true);
                                buttonMasTresVisitante.setEnabled(true);
                                buttonMenosUnoVisitante.setEnabled(true);
                                buttonMenosDosVisitante.setEnabled(true);
                                buttonMenosTresVisitante.setEnabled(true);
                                fab.setVisibility(View.VISIBLE);
                            }
                        });
                builder.create();
                builder.show();

            }
        });

        fab.setVisibility(View.GONE);*/

        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
//        FirebaseUser user = mAuth.getCurrentUser();

        partido = (TipoPartido) getIntent().getExtras().getSerializable("Partido");
        myRef.child("Partidos").child(partido.DataBaseID).child("Status").setValue("JUGANDO");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Retransmisiones").child(partido.DataBaseID).exists())
                    myRef.child("Retransmisiones").child(partido.DataBaseID).removeValue();


                iterable = dataSnapshot.child("Equipos").getChildren();

                ArrayList<TipoEquipo> arrayListTipoEquipo = new ArrayList<TipoEquipo>();

                for (DataSnapshot ds : iterable) {
                    TipoEquipo tipoEquipo = ds.getValue(TipoEquipo.class);
                    tipoEquipo.DataBaseID = ds.getKey();
                    arrayListTipoEquipo.add(tipoEquipo);
                }


                for (TipoEquipo te : arrayListTipoEquipo) {
                    if (te.Nombre.equals(partido.Local))
                        equipoLocal = te;
                    else if (te.Nombre.equals(partido.Visitante))
                        equipoVisitante = te;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        retransmision = new TipoRetransmision();
        retransmision.Local = partido.Local;
        retransmision.Visitante = partido.Visitante;
        retransmision.DatabaseID = partido.DataBaseID;


        tiempoRestante = 12 * 60 * 1000;
        marcadorLocal = 0;
        faltasLocal = 0;
        marcadorVisitante = 0;
        faltasVisitante = 0;
        cuarto = 1;

        txtViewNombreLocal = findViewById(R.id.txtViewNombreLocal);
        textViewMarcadorLocal = findViewById(R.id.textViewMarcadorLocal);
        textViewTiempoPartido = findViewById(R.id.textViewTiempoPartido);
        textViewCuartoPartido = findViewById(R.id.textViewCuartoPartido);
        txtViewNombreVisitante = findViewById(R.id.txtViewNombreVisitante);
        textViewMarcadorVisitante = findViewById(R.id.textViewMarcadorVisitante);

        buttonMenosUnoLocal = findViewById(R.id.buttonMenosUnoLocal);
        buttonMenosDosLocal = findViewById(R.id.buttonMenosDosLocal);
        buttonMenosTresLocal = findViewById(R.id.buttonMenosTresLocal);
        buttonMasUnoLocal = findViewById(R.id.buttonMasUnoLocal);
        buttonMasDosLocal = findViewById(R.id.buttonMasDosLocal);
        buttonMasTresLocal = findViewById(R.id.buttonMasTresLocal);
        buttonMenosFaltaLocal = findViewById(R.id.buttonMenosFaltaLocal);
        buttonMasFaltaLocal = findViewById(R.id.buttonMasFaltaLocal);
        buttonStopTiempo = findViewById(R.id.buttonStopTiempo);
        buttonStartPauseTiempo = findViewById(R.id.buttonStartPauseTiempo);
        buttonMenosUnoVisitante = findViewById(R.id.buttonMenosUnoVisitante);
        buttonMenosDosVisitante = findViewById(R.id.buttonMenosDosVisitante);
        buttonMenosTresVisitante = findViewById(R.id.buttonMenosTresVisitante);
        buttonMasUnoVisitante = findViewById(R.id.buttonMasUnoVisitante);
        buttonMasDosVisitante = findViewById(R.id.buttonMasDosVisitante);
        buttonMasTresVisitante = findViewById(R.id.buttonMasTresVisitante);
        buttonMenosFaltaVisitante = findViewById(R.id.buttonMenosFaltaVisitante);
        buttonMasFaltaVisitante = findViewById(R.id.buttonMasFaltaVisitante);

        radioButtonFaltaUnoLocal = findViewById(R.id.radioButtonFaltaUnoLocal);
        radioButtonFaltaDosLocal = findViewById(R.id.radioButtonFaltaDosLocal);
        radioButtonFaltaTresLocal = findViewById(R.id.radioButtonFaltaTresLocal);
        radioButtonFaltaCuatroLocal = findViewById(R.id.radioButtonFaltaCuatroLocal);
        radioButtonFaltaCincoLocal = findViewById(R.id.radioButtonFaltaCincoLocal);
        radioButtonFaltaUnoVisitante = findViewById(R.id.radioButtonFaltaUnoVisitante);
        radioButtonFaltaDosVisitante = findViewById(R.id.radioButtonFaltaDosVisitante);
        radioButtonFaltaTresVisitante = findViewById(R.id.radioButtonFaltaTresVisitante);
        radioButtonFaltaCuatroVisitante = findViewById(R.id.radioButtonFaltaCuatroVisitante);
        radioButtonFaltaCincoVisitante = findViewById(R.id.radioButtonFaltaCincoVisitante);

        txtViewNombreLocal.setText(partido.Local);
        txtViewNombreVisitante.setText(partido.Visitante);


        buttonStartPauseTiempo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tiempoCorriendo) {
                    tiempoCorriendo = false;
                    buttonStartPauseTiempo.setText("START");
                    pararContador(countDownTimer);
                    retransmision.Tiempo = "TIEMPO MUERTO";
                    myRef.child("Retransmisiones").child(retransmision.DatabaseID).setValue(retransmision);
                } else {
                    actualizaCuarto();
                    countDownTimer = iniciarContador(tiempoRestante, 100);
                    countDownTimer.start();
                    tiempoCorriendo = true;
                    buttonStartPauseTiempo.setText("PAUSE");
                }
            }
        });

        buttonMenosFaltaLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaFaltas(-1, "L");
            }
        });

        buttonMasFaltaLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaFaltas(1, "L");
            }
        });

        buttonMenosFaltaVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaFaltas(-1, "V");
            }
        });

        buttonMasFaltaVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaFaltas(1, "V");
            }
        });

        buttonMenosUnoLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(-1, "L");
                actualizaMarcador("L");
            }
        });

        buttonMenosDosLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(-2, "L");
                actualizaMarcador("L");
            }
        });

        buttonMenosTresLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(-3, "L");
                actualizaMarcador("L");
            }
        });

        buttonMasUnoLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(1, "L");
                actualizaMarcador("L");
            }
        });

        buttonMasDosLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(2, "L");
                actualizaMarcador("L");
            }
        });

        buttonMasTresLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(3, "L");
                actualizaMarcador("L");
            }
        });

        buttonMenosUnoVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(-1, "V");
                actualizaMarcador("V");
            }
        });

        buttonMenosDosVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(-2, "V");
                actualizaMarcador("V");
            }
        });

        buttonMenosTresVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(-3, "V");
                actualizaMarcador("V");
            }
        });

        buttonMasUnoVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(1, "V");
                actualizaMarcador("V");
            }
        });

        buttonMasDosVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(2, "V");
                actualizaMarcador("V");
            }
        });

        buttonMasTresVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumaPuntos(3, "V");
                actualizaMarcador("V");
            }
        });

        buttonStopTiempo.setOnClickListener(view -> {
            if (marcadorLocal != marcadorVisitante) {
                showSaveDialog();
            }
        });


    }

    private void sumaPuntos(int puntos, String equipo) {
        if (equipo.equals("L"))
            marcadorLocal = (marcadorLocal + puntos) > 0 ? marcadorLocal + puntos : 0;
        else
            marcadorVisitante = (marcadorVisitante + puntos) > 0 ? marcadorVisitante + puntos : 0;
    }

    private void sumaFaltas(int faltas, String equipo) {
        if (equipo.equals("L")) {
            faltasLocal += faltas;
            if (faltasLocal < 0)
                faltasLocal = 0;
            if (faltasLocal > 5)
                faltasLocal = 5;
            switch (faltasLocal) {
                default:
                case 5:
                    radioButtonFaltaCincoLocal.setChecked(true);
                    radioButtonFaltaCuatroLocal.setChecked(true);
                    radioButtonFaltaTresLocal.setChecked(true);
                    radioButtonFaltaDosLocal.setChecked(true);
                    radioButtonFaltaUnoLocal.setChecked(true);
                    break;
                case 4:
                    radioButtonFaltaCincoLocal.setChecked(false);
                    radioButtonFaltaCuatroLocal.setChecked(true);
                    radioButtonFaltaTresLocal.setChecked(true);
                    radioButtonFaltaDosLocal.setChecked(true);
                    radioButtonFaltaUnoLocal.setChecked(true);
                    break;
                case 3:
                    radioButtonFaltaCincoLocal.setChecked(false);
                    radioButtonFaltaCuatroLocal.setChecked(false);
                    radioButtonFaltaTresLocal.setChecked(true);
                    radioButtonFaltaDosLocal.setChecked(true);
                    radioButtonFaltaUnoLocal.setChecked(true);
                    break;
                case 2:
                    radioButtonFaltaCincoLocal.setChecked(false);
                    radioButtonFaltaCuatroLocal.setChecked(false);
                    radioButtonFaltaTresLocal.setChecked(false);
                    radioButtonFaltaDosLocal.setChecked(true);
                    radioButtonFaltaUnoLocal.setChecked(true);
                    break;
                case 1:
                    radioButtonFaltaCincoLocal.setChecked(false);
                    radioButtonFaltaCuatroLocal.setChecked(false);
                    radioButtonFaltaTresLocal.setChecked(false);
                    radioButtonFaltaDosLocal.setChecked(false);
                    radioButtonFaltaUnoLocal.setChecked(true);
                    break;
                case 0:
                    radioButtonFaltaCincoLocal.setChecked(false);
                    radioButtonFaltaCuatroLocal.setChecked(false);
                    radioButtonFaltaTresLocal.setChecked(false);
                    radioButtonFaltaDosLocal.setChecked(false);
                    radioButtonFaltaUnoLocal.setChecked(false);
                    break;
            }
        } else {
            faltasVisitante += faltas;
            if (faltasVisitante < 0)
                faltasVisitante = 0;
            if (faltasVisitante > 5)
                faltasVisitante = 5;
            switch (faltasVisitante) {
                default:
                case 5:
                    radioButtonFaltaCincoVisitante.setChecked(true);
                    radioButtonFaltaCuatroVisitante.setChecked(true);
                    radioButtonFaltaTresVisitante.setChecked(true);
                    radioButtonFaltaDosVisitante.setChecked(true);
                    radioButtonFaltaUnoVisitante.setChecked(true);
                    break;
                case 4:
                    radioButtonFaltaCincoVisitante.setChecked(false);
                    radioButtonFaltaCuatroVisitante.setChecked(true);
                    radioButtonFaltaTresVisitante.setChecked(true);
                    radioButtonFaltaDosVisitante.setChecked(true);
                    radioButtonFaltaUnoVisitante.setChecked(true);
                    break;
                case 3:
                    radioButtonFaltaCincoVisitante.setChecked(false);
                    radioButtonFaltaCuatroVisitante.setChecked(false);
                    radioButtonFaltaTresVisitante.setChecked(true);
                    radioButtonFaltaDosVisitante.setChecked(true);
                    radioButtonFaltaUnoVisitante.setChecked(true);
                    break;
                case 2:
                    radioButtonFaltaCincoVisitante.setChecked(false);
                    radioButtonFaltaCuatroVisitante.setChecked(false);
                    radioButtonFaltaTresVisitante.setChecked(false);
                    radioButtonFaltaDosVisitante.setChecked(true);
                    radioButtonFaltaUnoVisitante.setChecked(true);
                    break;
                case 1:
                    radioButtonFaltaCincoVisitante.setChecked(false);
                    radioButtonFaltaCuatroVisitante.setChecked(false);
                    radioButtonFaltaTresVisitante.setChecked(false);
                    radioButtonFaltaDosVisitante.setChecked(false);
                    radioButtonFaltaUnoVisitante.setChecked(true);
                    break;
                case 0:
                    radioButtonFaltaCincoVisitante.setChecked(false);
                    radioButtonFaltaCuatroVisitante.setChecked(false);
                    radioButtonFaltaTresVisitante.setChecked(false);
                    radioButtonFaltaDosVisitante.setChecked(false);
                    radioButtonFaltaUnoVisitante.setChecked(false);
                    break;
            }
        }

        retransmision.FaltasLocal = faltasLocal;
        retransmision.FaltasVisitante = faltasVisitante;
    }

    private void actualizaMarcador(String equipo) {
        if (equipo.equals("L"))
            textViewMarcadorLocal.setText("" + marcadorLocal);
        else
            textViewMarcadorVisitante.setText("" + marcadorVisitante);

        retransmision.ResultadoLocal = marcadorLocal;
        retransmision.ResultadoVisitante = marcadorVisitante;
    }

    private void actualizaCuarto() {
        if (cuarto <= 4) {
            textViewCuartoPartido.setText(cuarto + "T");
            retransmision.Cuarto = cuarto + "T";
        } else if (cuarto > 4 && marcadorLocal == marcadorVisitante) {
            textViewCuartoPartido.setText("OT " + (cuarto - 4));
            retransmision.Cuarto = "OT" + (cuarto - 4);
        } else
            textViewCuartoPartido.setText("FIN");
    }

    private CountDownTimer iniciarContador(long inicial, long tick) {
        return new CountDownTimer(inicial, tick) {

            public void onTick(long millisUntilFinished) {
                textViewTiempoPartido.setText(String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                tiempoRestante = millisUntilFinished;
                retransmision.Tiempo = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                myRef.child("Retransmisiones").child(retransmision.DatabaseID).setValue(retransmision);
            }

            public void onFinish() {
                MediaPlayer mp = MediaPlayer.create(context, R.raw.nautical022);
                mp.setLooping(false);
                mp.setVolume(100, 100);
                mp.start();
                tiempoCorriendo = false;
                buttonStartPauseTiempo.setText("START");
                cuarto++;
                faltasLocal = 0;
                faltasVisitante = 0;
                if (cuarto <= 4)
                    tiempoRestante = 12 * 60 * 1000;
                else
                    tiempoRestante = 5 * 60 * 1000;
                if (cuarto > 4) {
                    if (marcadorLocal != marcadorVisitante) {
                        buttonStartPauseTiempo.setEnabled(false);

                        buttonMasUnoLocal.setEnabled(false);
                        buttonMasDosLocal.setEnabled(false);
                        buttonMasTresLocal.setEnabled(false);
                        buttonMenosUnoLocal.setEnabled(false);
                        buttonMenosDosLocal.setEnabled(false);
                        buttonMenosTresLocal.setEnabled(false);

                        buttonMasUnoVisitante.setEnabled(false);
                        buttonMasDosVisitante.setEnabled(false);
                        buttonMasTresVisitante.setEnabled(false);
                        buttonMenosUnoVisitante.setEnabled(false);
                        buttonMenosDosVisitante.setEnabled(false);
                        buttonMenosTresVisitante.setEnabled(false);

                        showSaveDialog();

                    }
                }
            }
        };
    }

    private void pararContador(CountDownTimer contador) {
        if (contador != null) {
            contador.cancel();
            contador = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSaveDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("¿Guardar el resultado " + partido.Local + " " + marcadorLocal + " - " + partido.Visitante + " " + marcadorVisitante + "?")
                .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        partido.Status = "JUGADO";
                        partido.ResultadoLocal = "" + marcadorLocal;
                        partido.ResultadoVisitante = "" + marcadorVisitante;

                        myRef.child("Partidos").child(partido.DataBaseID).setValue(partido);

                        if (equipoLocal.Clasificacion == null) {
                            equipoLocal.Clasificacion = TipoClasificacion.obtenClasificacionEquipo(equipoLocal, new ArrayList<>());
                        }

                        if (equipoVisitante.Clasificacion == null) {
                            equipoVisitante.Clasificacion = TipoClasificacion.obtenClasificacionEquipo(equipoVisitante, new ArrayList<>());
                        }

                        if (Integer.parseInt(partido.ResultadoLocal) > Integer.parseInt(partido.ResultadoVisitante)) {
                            equipoLocal.Clasificacion.Victorias++;
                            equipoVisitante.Clasificacion.Derrotas++;
                        } else {
                            equipoLocal.Clasificacion.Derrotas++;
                            equipoVisitante.Clasificacion.Victorias++;
                        }

                        equipoLocal.Clasificacion.CanastasFavor += Integer.parseInt(partido.ResultadoLocal);
                        equipoLocal.Clasificacion.CanastasContra += Integer.parseInt(partido.ResultadoVisitante);
                        equipoVisitante.Clasificacion.CanastasFavor += Integer.parseInt(partido.ResultadoVisitante);
                        equipoVisitante.Clasificacion.CanastasContra += Integer.parseInt(partido.ResultadoLocal);
                        equipoLocal.Clasificacion.Puntos = 2 * equipoLocal.Clasificacion.Victorias + equipoLocal.Clasificacion.Derrotas;
                        equipoVisitante.Clasificacion.Puntos = 2 * equipoVisitante.Clasificacion.Victorias + equipoVisitante.Clasificacion.Derrotas;

                        myRef.child("Equipos").child(equipoLocal.DataBaseID).setValue(equipoLocal);
                        myRef.child("Equipos").child(equipoVisitante.DataBaseID).setValue(equipoVisitante);

                        Intent i = new Intent(context, Valoracion.class);
                        i.putExtra("Partido", partido);

                        myRef = null;
                        partido = null;

                        startActivity(i);
                        finish();
                    }
                })
                .setNegativeButton("Cancelar", (dialog, id) -> {
                    Toast.makeText(context, "Revise el resultado y pulse el boton inferior", Toast.LENGTH_LONG).show();

                    buttonMasUnoLocal.setEnabled(true);
                    buttonMasDosLocal.setEnabled(true);
                    buttonMasTresLocal.setEnabled(true);
                    buttonMenosUnoLocal.setEnabled(true);
                    buttonMenosDosLocal.setEnabled(true);
                    buttonMenosTresLocal.setEnabled(true);

                    buttonMasUnoVisitante.setEnabled(true);
                    buttonMasDosVisitante.setEnabled(true);
                    buttonMasTresVisitante.setEnabled(true);
                    buttonMenosUnoVisitante.setEnabled(true);
                    buttonMenosDosVisitante.setEnabled(true);
                    buttonMenosTresVisitante.setEnabled(true);
                    fab.setVisibility(View.VISIBLE);
                });
        builder.create();
        builder.show();
    }

}


