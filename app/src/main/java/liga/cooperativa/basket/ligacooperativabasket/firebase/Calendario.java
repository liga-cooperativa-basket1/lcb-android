package liga.cooperativa.basket.ligacooperativabasket.firebase;

import static liga.cooperativa.basket.ligacooperativabasket.firebase.utils.TypeExtensionsKt.getTeamLogo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import liga.cooperativa.basket.ligacooperativabasket.BR;
import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.databinding.ItemGameBinding;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.firebase.utils.TypeExtensionsKt;


public class Calendario extends Fragment {

    private ArrayList<TipoPartido> arrayListaTipoPartidos = new ArrayList<>();
    private RecyclerView recyclerView;
    public FirebaseAuth mAuth;
    public Iterable<DataSnapshot> iterable;
    public String miEquipoSt;
    public DatabaseReference myRef;
    public TextView noMatchesText;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_calendario, container, false);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        recyclerView = view.findViewById(R.id.recyclerView);
        noMatchesText = view.findViewById(R.id.textoSinPartidos);

        myRef = FirebaseDatabase.getInstance().getReference();


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //showProgressDialog("Actualizando...");
                arrayListaTipoPartidos = new ArrayList<>();

                miEquipoSt = (String) dataSnapshot.child("Usuarios").child(mAuth.getUid()).child("Equipo").getValue();

                if (!dataSnapshot.child("Partidos").exists()) {
                    noMatchesText.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else if (dataSnapshot.child("Partidos").getChildrenCount() > 0) {
                    iterable = dataSnapshot.child("Partidos").getChildren();


                    for (DataSnapshot ds : iterable) {
                        TipoPartido p = ds.getValue(TipoPartido.class);
                        if (p.Status.equals("FIJADO") || p.Status.equals("JUGANDO")) {
                            p.DataBaseID = ds.getKey();

                            Calendar fechaPartido = Calendar.getInstance();
                            fechaPartido.setTime(p.getMatchDate());
                            Calendar fechaActual = Calendar.getInstance();
                            if (!fechaPartido.before(fechaActual))
                                arrayListaTipoPartidos.add(p);
                        }
                    }

                    Collections.sort(arrayListaTipoPartidos, (p2, p1) ->
                            p1.getMatchDate().compareTo(p2.getMatchDate()) * -1
                    );

                    if (arrayListaTipoPartidos.isEmpty()) {
                        noMatchesText.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                    new LastAdapter(TypeExtensionsKt.withHeaders(arrayListaTipoPartidos), BR.game)
                            .map(String.class, R.layout.item_date)
                            .map(TipoPartido.class, new ItemType<ItemGameBinding>(R.layout.item_game) {
                                @Override
                                public void onBind(@NotNull Holder<ItemGameBinding> holder) {
                                    Object item = TypeExtensionsKt.withHeaders(arrayListaTipoPartidos).get(holder.getBindingAdapterPosition());
                                    if (item instanceof TipoPartido) {
                                        TipoPartido game = (TipoPartido) item;
                                        holder.getBinding().setIsCurrentTeam(game.Local.equals(miEquipoSt) || game.Visitante.equals(miEquipoSt));
                                        holder.getBinding().setLogoHome(getTeamLogo(dataSnapshot, storage, game.Local));
                                        holder.getBinding().setLogoAway(getTeamLogo(dataSnapshot, storage, game.Visitante));
                                    }
                                }
                            })
                            .into(recyclerView);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return view;
    }

}
