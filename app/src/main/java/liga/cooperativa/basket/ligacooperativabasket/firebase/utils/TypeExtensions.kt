package liga.cooperativa.basket.ligacooperativabasket.firebase.utils

import android.graphics.Bitmap
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.firebase.database.DataSnapshot
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import liga.cooperativa.basket.ligacooperativabasket.R
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido
import java.util.*

fun ArrayList<TipoPartido>?.withHeaders(): ArrayList<Any> {
    val result = arrayListOf<Any>()
    this?.forEach {
        val date = it.dateWithFormat()
        result.addAll(
            when (date) {
                !in result -> listOf(date, it)
                else -> listOf(it)
            }
        )
    }
    return result
}

@BindingAdapter("logo")
fun ImageView.setLogoResource(storageReference: StorageReference?) {
    Glide.with(context)
        .using(FirebaseImageLoader())
        .load(storageReference)
        .asBitmap().centerCrop().into(object : BitmapImageViewTarget(this) {
            override fun setResource(resource: Bitmap) {
                val circularBitmapDrawable =
                    RoundedBitmapDrawableFactory.create(context.resources, resource)
                circularBitmapDrawable.isCircular = true
                setImageDrawable(circularBitmapDrawable)
            }
        })
}

fun getTeamLogo(dataSnapshot: DataSnapshot, storage: FirebaseStorage, teamName: String): StorageReference? {
    val iterableTeams = dataSnapshot.child("Equipos").children
    for (teamData in iterableTeams) {
        val team = teamData.getValue(TipoEquipo::class.java)
        if (team != null && team.Nombre == teamName) {
            return storage.reference.child(team.LinkLogo)
        }
    }
    return null
}

@BindingAdapter("state")
fun TextView.setStateColor(game: TipoPartido) {
    when {
        game.Status == "JUGADO" && !game.Valoracion.isCompleted -> setTextColor(resources.getColor(R.color.azul))
        game.matchDate.isExpired() && game.Status == "FIJADO" -> setTextColor(resources.getColor(R.color.rojo))
        else -> setTextColor(resources.getColor(R.color.colorPrimary))
    }
}

fun Date.isExpired(): Boolean {
    val gameDate = Calendar.getInstance()
    gameDate.time = this
    return gameDate.before(Calendar.getInstance())
}
