package liga.cooperativa.basket.ligacooperativabasket.firebase.teams

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import liga.cooperativa.basket.ligacooperativabasket.R
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo

class LeagueTeamsFragment : Fragment() {

    @SuppressLint("CheckResult")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_league_teams_list, container, false)

        val teamsReference = FirebaseDatabase.getInstance().reference.child("Equipos")
        teamsReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val equipos = snapshot.children.mapNotNull { it.getValue(TipoEquipo::class.java) }
                view.findViewById<RecyclerView>(R.id.list).adapter = TeamRecyclerViewAdapter(equipos)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(error.details, error.message)
            }
        })
        view.findViewById<FloatingActionButton>(R.id.addButton).setOnClickListener {
            MaterialDialog(requireContext()).show {
                title(text = "Nombre Equipo")
                input { _, text ->
                    // Text submitted with the action button
                    teamsReference.push().setValue(TipoEquipo(text.toString()))
                }
                positiveButton(R.string.submit)
            }
        }
        return view
    }

    fun newTeamDialog() {

    }
}