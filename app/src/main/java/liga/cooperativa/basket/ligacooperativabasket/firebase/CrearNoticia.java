package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoNoticia;


public class CrearNoticia extends BaseActivity {

    TextView titulo;
    TextView cuerpo;
    Button botonGuardar;
    TipoNoticia tipoNoticiaRecibida;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            startActivity(new Intent(context, Noticias.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_crear_noticia);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(user.getEmail());
        textViewNombreUsuario.setText(user.getDisplayName());

        if (getIntent().getExtras() != null)
            tipoNoticiaRecibida = (TipoNoticia) getIntent().getExtras().getSerializable("Noticia");
        else
            tipoNoticiaRecibida = null;

        myRef = FirebaseDatabase.getInstance().getReference();


        titulo = findViewById(R.id.titulo);
        cuerpo = findViewById(R.id.cuerpo);
        botonGuardar = findViewById(R.id.botonGuardar);

        if (tipoNoticiaRecibida != null) {
            titulo.setText(tipoNoticiaRecibida.Titulo);
            cuerpo.setText(tipoNoticiaRecibida.Cuerpo);
        }

        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressDialog("Guardando Noticia...");
                TipoNoticia tipoNoticia = new TipoNoticia();
                tipoNoticia.Autor = mAuth.getCurrentUser().getDisplayName();
                tipoNoticia.AutorID = mAuth.getUid();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                tipoNoticia.Fecha = sdf.format(c.getTime());

                if (titulo.getText().toString().isEmpty() || cuerpo.getText().toString().isEmpty())
                    return;
                tipoNoticia.Titulo = titulo.getText().toString();
                tipoNoticia.Cuerpo = cuerpo.getText().toString();
                if (tipoNoticia.DatabaseId != null)
                    myRef.child("Noticias").child(tipoNoticia.DatabaseId).setValue(tipoNoticia);
                else
                    myRef.child("Noticias").push().setValue(tipoNoticia);

                hideProgressDialog();

                startActivity(new Intent(context, Noticias.class));
                finish();
            }
        });


    }


}
