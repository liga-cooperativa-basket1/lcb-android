package liga.cooperativa.basket.ligacooperativabasket.firebase;


import android.content.Intent;
import android.os.Bundle;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.firebase.utils.MaterialDialogsKt;
import liga.cooperativa.basket.ligacooperativabasket.listados.AdaptadorListas;


public class FijarPartidoRival extends BaseActivity {

    public static int año = -1;
    public static int mes = -1;
    public static int dia = -1;
    public static int hora = -1;
    public static int minutos = -1;
    private static ListView listViewElegirEquipo;
    private static ArrayList<TipoPartido> listaTipoPartidos = new ArrayList<>();
    private static TipoEquipo tipoEquipoElegido;
    private static TipoEquipo tipoEquipoPropio;
    private ArrayList<TipoEquipo> listaTipoEquipos = new ArrayList<>();
    private ArrayList<TipoEquipo> listaEquiposGrupo = new ArrayList<>();
    private ArrayList<TipoPartido> listaPartidosEquipo = new ArrayList<>();
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_fijar_rival);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        año = -1;
        mes = -1;
        dia = -1;
        hora = -1;
        minutos = -1;
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                showProgressDialog("Actualizando rivales...");

                listaEquiposGrupo = new ArrayList<>();
                listaTipoPartidos = new ArrayList<>();
                listaPartidosEquipo = new ArrayList<>();
                listaEquiposGrupo = new ArrayList<>();

                if (!dataSnapshot.child("Equipos").exists())
                    return;
                if (dataSnapshot.child("Equipos").getChildrenCount() < 1)
                    return;

                iterable = dataSnapshot.child("Equipos").getChildren();


                for (DataSnapshot ds : iterable) {
                    TipoEquipo e = ds.getValue(TipoEquipo.class);
                    listaTipoEquipos.add(e);
                }

                Collections.sort(listaTipoEquipos, new Comparator<TipoEquipo>() {
                    @Override
                    public int compare(TipoEquipo e2, TipoEquipo e1) {
                        return e1.Nombre.compareTo(e2.Nombre) * -1;
                    }
                });

                tipoEquipoPropio = TipoEquipo.obtenerEquipoPorNombre(dataSnapshot.child("Usuarios").child(mAuth.getUid()).child("Equipo").getValue().toString(), listaTipoEquipos);
                listaEquiposGrupo = TipoEquipo.obtenerEquiposGrupo(tipoEquipoPropio, listaTipoEquipos);

                iterable = dataSnapshot.child("Partidos").getChildren();
                for (DataSnapshot ds : iterable) {
                    TipoPartido p = ds.getValue(TipoPartido.class);
                    listaTipoPartidos.add(p);
                }

//                iterable = dataSnapshot.child("Canchas").getChildren();
//                for (DataSnapshot ds : iterable) {
//                    TipoCancha c = ds.getValue(TipoCancha.class);
//                    listaTipoCanchas.add(c);
//                }

                listaPartidosEquipo = TipoPartido.obtenerPartidosEquipo(tipoEquipoPropio, listaTipoPartidos);
                listaEquiposGrupo = TipoEquipo.obtenerEquiposPorFijar(listaEquiposGrupo, listaPartidosEquipo);

                listViewElegirEquipo = findViewById(R.id.listaCalendario);

                if (listaEquiposGrupo.isEmpty()) {
                    listViewElegirEquipo.setVisibility(View.GONE);
                    TextView textoSinPartidos = findViewById(R.id.textoSinPartidos);
                    textoSinPartidos.setVisibility(View.VISIBLE);
                }


                listViewElegirEquipo.setAdapter(new AdaptadorListas(context, R.layout.elem_lista_elegirequipo, listaEquiposGrupo) {
                    @Override
                    public void onEntrada(Object entrada, View view) {
                        if (entrada != null) {
                            try {
                                TipoEquipo tipoEquipo = (TipoEquipo) entrada;

                                FirebaseStorage storage = FirebaseStorage.getInstance();
                                StorageReference storageReference = storage.getReference().child(tipoEquipo.LinkLogo);

                                ImageView logo_equipo = view.findViewById(R.id.logo_equipo);
                                if (logo_equipo != null && storageReference != null)
                                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_equipo);

                                TextView textViewPartido = view.findViewById(R.id.nombre_equipo);
                                if (textViewPartido != null)
                                    textViewPartido.setText(tipoEquipo.Nombre);

                            } catch (Exception ex) {
                                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

                listViewElegirEquipo.setOnItemClickListener((adapterView, view, position, l) -> {

                    tipoEquipoElegido = listaEquiposGrupo.get(position);
                    MaterialDialog materialDialog = new MaterialDialog(context, MaterialDialog.getDEFAULT_BEHAVIOR());
                    MaterialDialogsKt.showDateTime(materialDialog, calendar -> {
                        Intent intent = new Intent(context, ElegirCanchaPartido.class);
                        intent.putExtra("Fecha", calendar);
                        intent.putExtra("Local", tipoEquipoPropio.Nombre);
                        intent.putExtra("Visitante", tipoEquipoElegido.Nombre);

                        startActivity(intent);
                        return null;
                    });
                });

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hora = hourOfDay;
            minutos = minute;

            if (dia != -1 && mes != -1 && año != -1) {
                Intent intent = new Intent(getApplicationContext(), ElegirCanchaPartido.class);
                Calendar c = Calendar.getInstance();
                c.set(año, mes, dia, hora, minutos);
                intent.putExtra("Fecha", c);
                intent.putExtra("Local", tipoEquipoPropio.Nombre);
                intent.putExtra("Visitante", tipoEquipoElegido.Nombre);

                startActivity(intent);
            }
        }
    }*/

    /*public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            año = year;
            mes = month;
            dia = day;

//            if (hora != -1 && minutos != -1) {
//                Intent intent = new Intent(getContext(), ElegirCanchaPartido.class);
//                Calendar c = Calendar.getInstance();
//                c.set(año, mes, dia, hora, minutos);
//                intent.putExtra("Fecha", c);
//                intent.putExtra("Local", tipoEquipoPropio.Nombre);
//                intent.putExtra("Visitante", tipoEquipoElegido.Nombre);
//
//                startActivity(intent);
//            }
        }
    }*/
}
