package liga.cooperativa.basket.ligacooperativabasket.firebase;


import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;


public class PlayOffs extends BaseActivity {

    private ArrayList<TipoEquipo> listaEquipos = new ArrayList<>();

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            startActivity(new Intent(context, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_playoffs);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(user.getEmail());
        textViewNombreUsuario.setText(user.getDisplayName());

        showProgressDialog("Obteniendo brackets...");

        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                listaEquipos = new ArrayList<>();

                if (!dataSnapshot.child("Equipos").exists())
                    return;
                if (dataSnapshot.child("Equipos").getChildrenCount() < 1)
                    return;

                iterable = dataSnapshot.child("Equipos").getChildren();


                for (DataSnapshot ds : iterable) {
                    TipoEquipo e = ds.getValue(TipoEquipo.class);
                    if (!e.LinkLogo.equals("Logos/SinLogo-App.png"))
                        listaEquipos.add(e);
                }

                ImageView logo_primero = findViewById(R.id.logo_primero);
                ImageView logo_segundo = findViewById(R.id.logo_segundo);
                ImageView logo_tercero = findViewById(R.id.logo_tercero);
                ImageView logo_cuarto = findViewById(R.id.logo_cuarto);
                ImageView logo_quinto = findViewById(R.id.logo_quinto);
                ImageView logo_sexto = findViewById(R.id.logo_sexto);
                ImageView logo_septimo = findViewById(R.id.logo_septimo);
                ImageView logo_octavo = findViewById(R.id.logo_octavo);
                ImageView logo_semis_primero = findViewById(R.id.logo_semis_primero);
                ImageView logo_semis_segundo = findViewById(R.id.logo_semis_segundo);
                ImageView logo_semis_tercero = findViewById(R.id.logo_semis_tercero);
                ImageView logo_semis_cuarto = findViewById(R.id.logo_semis_cuarto);
                ImageView logo_final_primero = findViewById(R.id.logo_final_primero);
                ImageView logo_final_segundo = findViewById(R.id.logo_final_segundo);


                TextView texto_primero = findViewById(R.id.texto_primero);
                TextView texto_segundo = findViewById(R.id.texto_segundo);
                TextView texto_tercero = findViewById(R.id.texto_tercero);
                TextView texto_cuarto = findViewById(R.id.texto_cuarto);
                TextView texto_quinto = findViewById(R.id.texto_quinto);
                TextView texto_sexto = findViewById(R.id.texto_sexto);
                TextView texto_septimo = findViewById(R.id.texto_septimo);
                TextView texto_octavo = findViewById(R.id.texto_octavo);
                TextView texto_semis_primero = findViewById(R.id.texto_semis_primero);
                TextView texto_semis_segundo = findViewById(R.id.texto_semis_segundo);
                TextView texto_semis_tercero = findViewById(R.id.texto_semis_tercero);
                TextView texto_semis_cuarto = findViewById(R.id.texto_semis_cuarto);
                TextView texto_final_primero = findViewById(R.id.texto_final_primero);
                TextView texto_final_segundo = findViewById(R.id.texto_final_segundo);

                int index = 0;

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_primero);
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_semis_primero);
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_final_primero);
                    texto_primero.setText("3");
                    texto_semis_primero.setText("3");
                    texto_final_primero.setText("?");
                }
                index++;

                storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_segundo);
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_semis_segundo);
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_final_segundo);
                    texto_segundo.setText("3");
                    texto_semis_segundo.setText("3");
                    texto_final_segundo.setText("?");
                }
                index++;

                storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_tercero);
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_semis_tercero);
                    texto_tercero.setText("3");
                    texto_semis_tercero.setText("2");
                }
                index++;

                storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_cuarto);
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_semis_cuarto);
                    texto_cuarto.setText("3");
                    texto_semis_cuarto.setText("2");
                }
                index++;

                storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_quinto);
                    texto_quinto.setText("2");
                }
                index++;

                storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_sexto);
                    texto_sexto.setText("2");
                }
                index++;

                storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_septimo);
                    texto_septimo.setText("2");
                }
                index++;

                storageReference = storage.getReference().child(listaEquipos.get(index).LinkLogo);
                if (storageReference != null) {
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_octavo);
                    texto_octavo.setText("2");
                }


                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
