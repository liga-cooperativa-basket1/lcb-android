package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import android.app.DialogFragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentContainerView;

import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoClasificacion;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoFase;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.listados.AdaptadorListas;

public class IniciarLiga extends BaseActivity {

    static int año, mes, dia = -1;
    static EditText et_fecha_fin, et_numero_grupos;
    Button boton_rehacer;
    TextView txt_fase, fecha_inicio, txt_temporada;
    Button botonCambioFase, boton_guardar;
    TipoFase fase = new TipoFase();
    private ArrayList<TipoEquipo> arrayListaTipoEquipos = new ArrayList<>();
    private ListView listaElegirEquipo;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            startActivity(new Intent(context, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_iniciar_liga);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(user.getEmail());
        textViewNombreUsuario.setText(user.getDisplayName());

        txt_fase = findViewById(R.id.txt_fase);
        txt_temporada = findViewById(R.id.txt_temporada);
        fecha_inicio = findViewById(R.id.fecha_inicio);
        et_fecha_fin = findViewById(R.id.et_fecha_fin);
        et_fecha_fin.setInputType(InputType.TYPE_NULL);
        et_numero_grupos = findViewById(R.id.et_numero_grupos);
        botonCambioFase = findViewById(R.id.botonCambioFase);
        boton_guardar = findViewById(R.id.boton_guardar);
        Button teamsButton = findViewById(R.id.teamsButton);
        ImageButton doneButton = findViewById(R.id.doneButton);
        teamsButton.setOnClickListener(view -> {
            view.setVisibility(View.GONE);
            doneButton.setVisibility(View.VISIBLE);
            findViewById(R.id.fragment_container_view).setVisibility(View.VISIBLE);
        });
        doneButton.setOnClickListener(view -> {
            view.setVisibility(View.GONE);
            teamsButton.setVisibility(View.VISIBLE);
            findViewById(R.id.fragment_container_view).setVisibility(View.GONE);
        });

        et_fecha_fin.setOnClickListener(view -> {
            DatePickerFragment datePicker = new DatePickerFragment();
            datePicker.show(getFragmentManager(), "datePicker");
        });

        myRef = FirebaseDatabase.getInstance().getReference();


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                Calendar c = Calendar.getInstance();
                fase.FechaInicio = "" + c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
                fase.Temporada = "" + c.get(Calendar.YEAR) + " / " + (c.get(Calendar.YEAR) + 1);

                fecha_inicio.setText(fase.FechaInicio);
                txt_temporada.setText(fase.Temporada);


                if (!dataSnapshot.child("Fase").exists()) {
                    txt_fase.setText("SIN INICAR LIGA");
                    et_fecha_fin.setHint("NO DISPONIBLE");
                    botonCambioFase.setText("INICIAR LIGA");

                } else {
                    fase = dataSnapshot.child("Fase").getValue(TipoFase.class);

                    Calendar cActual = Calendar.getInstance();
                    Calendar cFin = Calendar.getInstance();
                    cFin.set(Integer.parseInt(fase.FechaFin.split("/")[2]), Integer.parseInt(fase.FechaFin.split("/")[1]) - 1, Integer.parseInt(fase.FechaFin.split("/")[0]));

                    if (cActual.before(cFin)) {
                        fase = dataSnapshot.child("Fase").getValue(TipoFase.class);
                        txt_temporada.setText(fase.Temporada);
                        txt_fase.setText("Fase" + fase.Nombre);
                        fecha_inicio.setText(fase.FechaInicio);
                        et_fecha_fin.setText(fase.FechaFin);
                        et_fecha_fin.setEnabled(false);
                        botonCambioFase.setEnabled(false);
                    } else {
                        txt_fase.setText("Fase " + fase.Nombre);
                        et_fecha_fin.setText("");
                        botonCambioFase.setEnabled(true);
                    }
                }

                boton_guardar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myRef.child("Fase").setValue(fase);

                        for (TipoEquipo e : arrayListaTipoEquipos) {
                            myRef.child("Equipos").child(e.DataBaseID).setValue(e);
                            startActivity(new Intent(context, MainActivity.class));
                            finish();
                        }

                    }

                });

                botonCambioFase.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!dataSnapshot.child("Fase").exists()) { //INICIO DE LIGA
                            if (!et_numero_grupos.getText().toString().isEmpty() && !et_fecha_fin.getText().toString().isEmpty()) {
                                fase.Grupos = Integer.parseInt(et_numero_grupos.getText().toString());
                                fase.FechaFin = et_fecha_fin.getText().toString();
                            } else
                                return;

                            fase.Nombre = "1";
                            RelativeLayout relativeNumeroGrupos = findViewById(R.id.rl_fase);
                            RelativeLayout relativeListadoGrupos = findViewById(R.id.relativeListadoGrupos);
                            relativeNumeroGrupos.setVisibility(View.GONE);
                            relativeListadoGrupos.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Iniciando liga...", Toast.LENGTH_LONG).show();
                            iterable = dataSnapshot.child("Equipos").getChildren();


                            for (DataSnapshot ds : iterable) {
                                TipoEquipo e = ds.getValue(TipoEquipo.class);
                                if (e.Activo.equals("SI")) {
                                    e.DataBaseID = ds.getKey();
                                    arrayListaTipoEquipos.add(e);
                                }
                            }

                            arrayListaTipoEquipos = TipoEquipo.configuraGruposInicioLiga(arrayListaTipoEquipos, fase.Grupos);

                            Collections.sort(arrayListaTipoEquipos, new Comparator<TipoEquipo>() {
                                @Override
                                public int compare(TipoEquipo e2, TipoEquipo e1) {
                                    if (e2.Grupo == e1.Grupo)
                                        return e2.Nombre.compareTo(e1.Nombre);
                                    return e2.Grupo - e1.Grupo;
                                }
                            });

                            listaElegirEquipo = findViewById(R.id.listaCalendario);
                            listaElegirEquipo.setAdapter(new AdaptadorListas(context, R.layout.elem_lista_iniciar_liga, arrayListaTipoEquipos) {
                                @Override
                                public void onEntrada(Object entrada, View view) {
                                    if (entrada != null) {
                                        try {
                                            final TipoEquipo tipoEquipo = (TipoEquipo) entrada;

                                            FirebaseStorage storage = FirebaseStorage.getInstance();
                                            StorageReference storageReference = storage.getReference().child(tipoEquipo.LinkLogo);

                                            ImageView logo_equipo = view.findViewById(R.id.logo_equipo);
                                            if (logo_equipo != null && storageReference != null)
                                                Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_equipo);

                                            TextView textViewPartido = view.findViewById(R.id.nombre_equipo);
                                            if (textViewPartido != null)
                                                textViewPartido.setText(tipoEquipo.Nombre);

                                            TextView grupo_equipo = view.findViewById(R.id.grupo_equipo);
                                            if (grupo_equipo != null)
                                                grupo_equipo.setText("Gr: " + tipoEquipo.Grupo);


                                        } catch (Exception ex) {
                                            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            });
                        } else {
                            fase = dataSnapshot.child("Fase").getValue(TipoFase.class);


                            Calendar cActual = Calendar.getInstance();
                            Calendar cFin = Calendar.getInstance();
                            cFin.set(Integer.parseInt(fase.FechaFin.split("/")[2]), Integer.parseInt(fase.FechaFin.split("/")[1]) - 1, Integer.parseInt(fase.FechaFin.split("/")[0]));

                            if (cActual.before(cFin)) {
                                Toast.makeText(context, "FASE SIN FINALIZAR", Toast.LENGTH_LONG).show();
                                botonCambioFase.setEnabled(false);
                            } else {
                                botonCambioFase.setEnabled(true);

                                if (!et_numero_grupos.getText().toString().isEmpty() && !et_fecha_fin.getText().toString().isEmpty()) {
                                    fase.Grupos = Integer.parseInt(et_numero_grupos.getText().toString());
                                    fase.FechaFin = et_fecha_fin.getText().toString();
                                    fase.Nombre = (Integer.parseInt(fase.Nombre) + 1) + "";
                                } else
                                    return;

                                ArrayList<TipoPartido> partidos = new ArrayList<>();
                                Iterable<DataSnapshot> iterablePartidos = dataSnapshot.child("Partidos").getChildren();

                                for (DataSnapshot ds : iterablePartidos) {
                                    TipoPartido p = ds.getValue(TipoPartido.class);
                                    p.DataBaseID = ds.getKey();
                                    partidos.add(p);
                                }

                                Iterable<DataSnapshot> iterableEquipos = dataSnapshot.child("Equipos").getChildren();
                                ArrayList<TipoEquipo> equipos = new ArrayList<>();
                                for (DataSnapshot ds : iterableEquipos) {
                                    TipoEquipo e = ds.getValue(TipoEquipo.class);
                                    e.DataBaseID = ds.getKey();
                                    if (e.Activo.equals("SI"))
                                        equipos.add(e);
                                }

                                ArrayList<ArrayList<TipoEquipo>> equiposPorGrupo = TipoClasificacion.separaEquiposPorGrupo(equipos, partidos);

                                ArrayList<ArrayList<TipoEquipo>> gruposNuevaFase = new ArrayList<>();
                                for (int i = 0; i < fase.Grupos; i++)
                                    gruposNuevaFase.add(new ArrayList<TipoEquipo>());

                                int numeroEquipos = equipos.size();

                                int numeroEquiposGrupo = numeroEquipos / fase.Grupos;
                                int numeroGrupoActual = 0;

                                for (int i = 0; i < fase.Grupos; i++) {
                                    for (int j = 0; j < numeroEquiposGrupo; j++) {
                                        if (equiposPorGrupo.get(numeroGrupoActual).size() > 0) {
                                            TipoEquipo e = equiposPorGrupo.get(numeroGrupoActual).get(0);
                                            equiposPorGrupo.get(numeroGrupoActual).remove(0);
                                            gruposNuevaFase.get(i).add(e);
                                        }
                                        numeroGrupoActual++;
                                        numeroGrupoActual %= equiposPorGrupo.size();
                                    }
                                }

                                int i = 1;
                                for (ArrayList<TipoEquipo> ae : gruposNuevaFase) {
                                    for (TipoEquipo e : ae)
                                        e.Grupo = i;
                                    i++;
                                }

                                gruposNuevaFase = TipoClasificacion.calculaClasificacionCambioFase(gruposNuevaFase, partidos);

                                for (ArrayList<TipoEquipo> ae : gruposNuevaFase) {
                                    for (TipoEquipo e : ae) {
                                        myRef.child("Equipos").child(e.DataBaseID).child("Grupo").setValue(e.Grupo);
                                    }
                                }

                                ArrayList<TipoPartido> partidosNoEliminables = TipoPartido.obtenerPartidosJugados(gruposNuevaFase, partidos);

                                for (TipoPartido p : partidos) {
                                    if (partidosNoEliminables.contains(p))
                                        continue;
                                    p.Status = "HISTÓRICO";
                                    myRef.child("Partidos").child(p.DataBaseID).removeValue();
                                    p.DataBaseID = "SIN DEFINIR";
                                    myRef.child("Partidos Históricos").push().setValue(p);
                                }

                                for (ArrayList<TipoEquipo> ae : gruposNuevaFase) {
                                    for (TipoEquipo e : ae) {
                                        e.Clasificacion = TipoClasificacion.obtenClasificacionEquipo(e, partidosNoEliminables);
                                    }
                                }

                                for (ArrayList<TipoEquipo> ae : gruposNuevaFase) {
                                    for (TipoEquipo e : ae) {
                                        myRef.child("Equipos").child(e.DataBaseID).child("Clasificacion").setValue(e.Clasificacion);
                                    }
                                }
                                myRef.child("Fase").setValue(fase);
                                startActivity(new Intent(context, MainActivity.class));
                                finish();

                            }
                        }
                    }
                });


                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            año = year;
            mes = month;
            dia = day;
            et_fecha_fin.setText(day + "/" + (month + 1) + "/" + year);

        }
    }
}
