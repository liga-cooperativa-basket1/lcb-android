package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos // ktlint-disable filename

data class GameOptionItem(
    val title: String,
    val callback: () -> Unit
)
