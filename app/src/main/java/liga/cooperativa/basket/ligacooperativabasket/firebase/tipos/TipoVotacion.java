package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import java.util.ArrayList;
import java.util.Calendar;

public class TipoVotacion {
    public String Equipo;
    public int Voto;
    public String FechaActualizacion;

    public TipoVotacion() {

    }

    public static double obtenerVotacionesEquipo(String equipo, ArrayList<TipoUsuario> usuarios) {
        int suma = 0;
        int numeroVotos = 0;

        for (TipoUsuario usuario : usuarios) {
            if (usuario.Votaciones != null) {
                for (TipoVotacion votacion : usuario.Votaciones)
                    if (votacion.Equipo.equals(equipo)) {
                        suma += votacion.Voto;
                        numeroVotos++;
                    }
            }
        }

        if (numeroVotos == 0)
            return numeroVotos;

        return ((double) suma / numeroVotos);
    }

    public static int obtenerNumeroVotosEquipo(String equipo, ArrayList<TipoUsuario> usuarios) {
        int numeroVotos = 0;

        for (TipoUsuario usuario : usuarios) {
            if (usuario.Votaciones != null) {
                for (TipoVotacion votacion : usuario.Votaciones)
                    if (votacion.Equipo.equals(equipo))
                        numeroVotos++;
            }
        }

        return numeroVotos;
    }

    public static int miVotacion(String equipo, TipoUsuario usuario) {
        if (usuario.Votaciones != null)
            for (TipoVotacion votacion : usuario.Votaciones)
                if (votacion.Equipo.equals(equipo))
                    return votacion.Voto;
        return 0;
    }

    public static void actualizaVotacion(String equipo, TipoUsuario usuario, int voto) {
        Calendar c = Calendar.getInstance();
        if (usuario.Votaciones != null) {
            for (TipoVotacion votacion : usuario.Votaciones) {
                if (votacion.Equipo.equals(equipo)) {

                    votacion.FechaActualizacion = "" + c.get(Calendar.DAY_OF_MONTH)
                            + "/" + (c.get(Calendar.MONTH) + 1)
                            + "/" + c.get(Calendar.YEAR)
                            + " " + c.get(Calendar.HOUR_OF_DAY)
                            + ":" + c.get(Calendar.MINUTE) + ":"
                            + c.get(Calendar.SECOND);
                    votacion.Voto = voto;
                    return;
                }
            }
            TipoVotacion v = new TipoVotacion();
            v.Equipo = equipo;
            v.Voto = voto;
            v.FechaActualizacion = "" + c.get(Calendar.DAY_OF_MONTH)
                    + "/" + (c.get(Calendar.MONTH) + 1)
                    + "/" + c.get(Calendar.YEAR)
                    + " " + c.get(Calendar.HOUR_OF_DAY)
                    + ":" + c.get(Calendar.MINUTE) + ":"
                    + c.get(Calendar.SECOND);
            usuario.Votaciones.add(v);
            return;
        } else {
            usuario.Votaciones = new ArrayList<>();
            TipoVotacion v = new TipoVotacion();
            v.Equipo = equipo;
            v.Voto = voto;
            v.FechaActualizacion = "" + c.get(Calendar.DAY_OF_MONTH)
                    + "/" + (c.get(Calendar.MONTH) + 1)
                    + "/" + c.get(Calendar.YEAR)
                    + " " + c.get(Calendar.HOUR_OF_DAY)
                    + ":" + c.get(Calendar.MINUTE) + ":"
                    + c.get(Calendar.SECOND);
            usuario.Votaciones.add(v);
        }
    }
}