package liga.cooperativa.basket.ligacooperativabasket.firebase.tipos;

import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class TipoCancha {

    public boolean Activo;
    public String Bus;
    public String Direccion;
    public String Estado;
    public String HorarioIluminacion;
    public int Iluminacion;
    public String Informacion;
    public String InformeCanchas;
    public String LinkImagenes;
    public String Metro;
    public String Nombre;
    public int NumeroCanchas;

    public ImageView Imagen;
    public double PorcentajeUso;

    public TipoCancha() {
        PorcentajeUso = 0;
    }

    public TipoCancha(String name) {
        this.Nombre = name;
        this.NumeroCanchas = 1;
        this.Activo = true;
    }

    public static ArrayList<TipoCancha> obtenerCanchasLibresFecha(Calendar calendar, ArrayList<TipoPartido> tipoPartidos, ArrayList<TipoCancha> tipoCanchas) {
        Calendar calendarMas = Calendar.getInstance();
        calendarMas.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE) + 74);
        Calendar calendarMenos = Calendar.getInstance();
        calendarMenos.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE) - 74);

        ArrayList<TipoCancha> canchasLibres = new ArrayList<>();
        for (TipoCancha c : tipoCanchas) {
            for (int i = 0; i < c.NumeroCanchas; i++)
                canchasLibres.add(c);
        }

        for (TipoPartido p : tipoPartidos) {
            try {
                Calendar fechaPartido = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                fechaPartido.setTime(sdf.parse(p.Fecha));

                if (fechaPartido.after(calendarMenos) && fechaPartido.before(calendarMas))
                    TipoCancha.eliminarCanchaNombre(canchasLibres, p.Campo);
            } catch (Exception ex) {

            }
        }

        for (TipoPartido p : tipoPartidos) {
            TipoCancha c = TipoCancha.obtenerCanchaNombre(canchasLibres, p.Campo);
            if (c != null)
                if (canchasLibres.indexOf(c) != -1)
                    canchasLibres.get(canchasLibres.indexOf(c)).PorcentajeUso++;
        }
        for (TipoCancha c : canchasLibres) {
            c.PorcentajeUso = (c.PorcentajeUso / tipoPartidos.size()) * 100;
        }


        return canchasLibres;
    }

    private static void eliminarCanchaNombre(ArrayList<TipoCancha> tipoCanchas, String nombre) {
        for (int i = 0; i < tipoCanchas.size(); i++) {
            if (tipoCanchas.get(i).Nombre.equals(nombre)) {
                tipoCanchas.remove(i);
                return;
            }
        }
    }

    private static TipoCancha obtenerCanchaNombre(ArrayList<TipoCancha> tipoCanchas, String nombre) {
        for (TipoCancha c : tipoCanchas) {
            if (c.Nombre.equals(nombre))
                return c;
        }
        return null;
    }

    public static ArrayList<TipoCancha> eliminarDuplicados(ArrayList<TipoCancha> tipoCanchas) {
        ArrayList<TipoCancha> canchasLibres = new ArrayList<>();
        for (TipoCancha c : tipoCanchas) {
            if (canchasLibres.indexOf(c) == -1 && c.Activo)
                canchasLibres.add(c);
        }

        Collections.sort(canchasLibres, new Comparator<TipoCancha>() {
            @Override
            public int compare(TipoCancha tipoCancha, TipoCancha t1) {
                return (t1.Nombre.compareTo(tipoCancha.Nombre)) * -1;
            }
        });

        return canchasLibres;
    }

    public static String obtenerCanchaMasUsada(ArrayList<TipoPartido> partidos) {
        String canchaMasUsada = null;
        HashMap<String, Integer> canchas = new HashMap<>();
        for (TipoPartido partido : partidos) {
            if (canchas.containsKey(partido.Campo))
                canchas.put(partido.Campo, canchas.get(partido.Campo) + 1);
            else
                canchas.put(partido.Campo, 1);
        }

        int maximo = 0;
        for (TipoPartido partido : partidos) {
            if (canchas.get(partido.Campo) > maximo) {
                canchaMasUsada = partido.Campo;
                maximo = canchas.get(partido.Campo);
            }
        }


        return canchaMasUsada;
    }
}
