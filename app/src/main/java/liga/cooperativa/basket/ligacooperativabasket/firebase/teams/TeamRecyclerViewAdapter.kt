package liga.cooperativa.basket.ligacooperativabasket.firebase.teams

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import liga.cooperativa.basket.ligacooperativabasket.databinding.ItemTeamBinding
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo

class TeamRecyclerViewAdapter(
    private val values: List<TipoEquipo>,
) : RecyclerView.Adapter<TeamRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemTeamBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(values[position])
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(private val binding: ItemTeamBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TipoEquipo) {
            binding.team = item
        }
    }
}