package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoClasificacion;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;

public class IntroducirResultadoPartido extends BaseActivity {


    TipoPartido tipoPartidoSeleccionado;
    TextView local, visitante;
    ImageView logo_local, logo_visitante;
    EditText resultado_local, resultado_visitante;
    Button buttonGuardar;
    TipoEquipo equipoLocal = null;
    TipoEquipo equipoVisitante = null;
    private ArrayList<TipoEquipo> arrayListTipoEquipo = new ArrayList<>();

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            startActivity(new Intent(context, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_introducir_resultados);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(user.getEmail());
        textViewNombreUsuario.setText(user.getDisplayName());

        showProgressDialog("Obteniendo información...");

        try {
            tipoPartidoSeleccionado = (TipoPartido) getIntent().getExtras().getSerializable("Partido");
        } catch (Exception ex) {
            hideProgressDialog();
        }

        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {


                arrayListTipoEquipo = new ArrayList<>();

                iterable = dataSnapshot.child("Equipos").getChildren();


                for (DataSnapshot ds : iterable) {
                    TipoEquipo tipoEquipo = ds.getValue(TipoEquipo.class);
                    tipoEquipo.DataBaseID = ds.getKey();
                    arrayListTipoEquipo.add(tipoEquipo);
                }


                for (TipoEquipo te : arrayListTipoEquipo) {
                    if (te.Nombre.equals(tipoPartidoSeleccionado.Local))
                        equipoLocal = te;
                    else if (te.Nombre.equals(tipoPartidoSeleccionado.Visitante))
                        equipoVisitante = te;
                }

                local = findViewById(R.id.local);
                visitante = findViewById(R.id.visitante);
                logo_local = findViewById(R.id.logo_local);
                logo_visitante = findViewById(R.id.logo_visitante);
                resultado_local = findViewById(R.id.resultado_local);
                resultado_visitante = findViewById(R.id.resultado_visitante);
                buttonGuardar = findViewById(R.id.buttonGuardar);

                local.setText(equipoLocal.Nombre);
                visitante.setText(equipoVisitante.Nombre);


                /*LOGO LOCAL*/
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageReference = storage.getReference().child(equipoLocal.LinkLogo);

                if (storageReference != null)
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_local);

                /*LOGO VISITANTE*/
                storageReference = storage.getReference().child(equipoVisitante.LinkLogo);

                if (storageReference != null)
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_visitante);

                buttonGuardar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (resultado_local.getText().toString().isEmpty() || resultado_visitante.getText().toString().isEmpty())
                            return;

                        int resultadoLocal = Integer.parseInt(resultado_local.getText().toString());
                        int resultadoVisitante = Integer.parseInt(resultado_visitante.getText().toString());
                        if (resultadoLocal < 0 || resultadoVisitante < 0 || resultadoLocal == resultadoVisitante) {
                            Toast.makeText(context, "Revise el marcador", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (equipoLocal.Clasificacion == null) {
                            equipoLocal.Clasificacion = TipoClasificacion.obtenClasificacionEquipo(equipoLocal, new ArrayList<>());
                        }

                        if (equipoVisitante.Clasificacion == null) {
                            equipoVisitante.Clasificacion = TipoClasificacion.obtenClasificacionEquipo(equipoVisitante, new ArrayList<>());
                        }


                        /*myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).child("Status").setValue("JUGADO");
                        myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).child("ResultadoLocal").setValue("" + resultadoLocal);
                        myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).child("ResultadoVisitante").setValue("" + resultadoVisitante);
                        myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).child("ActualizadoPor").setValue(mAuth.getUid());*/

                        equipoLocal.Clasificacion.CanastasFavor += resultadoLocal;
                        equipoLocal.Clasificacion.CanastasContra += resultadoVisitante;
                        equipoLocal.Clasificacion.Victorias += resultadoLocal > resultadoVisitante ? 1 : 0;
                        equipoLocal.Clasificacion.Derrotas += resultadoLocal < resultadoVisitante ? 1 : 0;
                        equipoLocal.Clasificacion.Puntos = 2 * equipoLocal.Clasificacion.Victorias + equipoLocal.Clasificacion.Derrotas;

                        myRef.child("Equipos").child(equipoLocal.DataBaseID).setValue(equipoLocal);


                        equipoVisitante.Clasificacion.CanastasFavor += resultadoVisitante;
                        equipoVisitante.Clasificacion.CanastasContra += resultadoLocal;
                        equipoVisitante.Clasificacion.Victorias += resultadoLocal < resultadoVisitante ? 1 : 0;
                        equipoVisitante.Clasificacion.Derrotas += resultadoLocal > resultadoVisitante ? 1 : 0;
                        equipoVisitante.Clasificacion.Puntos = 2 * equipoVisitante.Clasificacion.Victorias + equipoVisitante.Clasificacion.Derrotas;

                        myRef.child("Equipos").child(equipoVisitante.DataBaseID).setValue(equipoVisitante);

                        /*myRef.child("Equipos").child(equipoVisitante.DataBaseID).child("Clasificacion").child("CanastasContra").setValue(equipoVisitante.Clasificacion.CanastasContra + resultadoLocal);
                        myRef.child("Equipos").child(equipoVisitante.DataBaseID).child("Clasificacion").child("CanastasFavor").setValue(equipoVisitante.Clasificacion.CanastasFavor + resultadoVisitante);
                        myRef.child("Equipos").child(equipoVisitante.DataBaseID).child("Clasificacion").child("Derrotas").setValue((resultadoLocal > resultadoVisitante) ? (equipoVisitante.Clasificacion.Derrotas + 1) : equipoVisitante.Clasificacion.Derrotas);
                        myRef.child("Equipos").child(equipoVisitante.DataBaseID).child("Clasificacion").child("Victorias").setValue((resultadoLocal < resultadoVisitante) ? (equipoVisitante.Clasificacion.Victorias + 1) : equipoVisitante.Clasificacion.Victorias);
                        myRef.child("Equipos").child(equipoVisitante.DataBaseID).child("Clasificacion").child("Puntos").setValue(2 * equipoVisitante.Clasificacion.Victorias + equipoVisitante.Clasificacion.Derrotas);*/

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        tipoPartidoSeleccionado.Status = "JUGADO";
                        tipoPartidoSeleccionado.ResultadoLocal = "" + resultadoLocal;
                        tipoPartidoSeleccionado.ResultadoVisitante = "" + resultadoVisitante;
                        tipoPartidoSeleccionado.ActualizadoPor = mAuth.getUid();
                        tipoPartidoSeleccionado.FechaActualizacion = sdf.format(c.getTime());

                        myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).setValue(tipoPartidoSeleccionado);

                        Intent i = new Intent(context, Valoracion.class);
                        i.putExtra("Partido", tipoPartidoSeleccionado);
                        startActivity(i);
                        finish();
                    }
                });


                hideProgressDialog();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo
            menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.listaCalendario) {
            tipoPartidoSeleccionado = (TipoPartido) ((ListView) v).getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_lista_partidos, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.introducir_resultado:

                return true;
            case R.id.cambiar_cancha:
                Intent intent = new Intent(context, ElegirCanchaPartido.class);
                String fecha = tipoPartidoSeleccionado.Fecha;
                int año = Integer.parseInt(fecha.split("/")[2].split(" ")[0]);
                int mes = Integer.parseInt(fecha.split("/")[1]) - 1;
                int dia = Integer.parseInt(fecha.split("/")[0]);
                int hora = Integer.parseInt(fecha.split(" ")[1].split(":")[0]);
                int minutos = Integer.parseInt(fecha.split(" ")[1].split(":")[1]);

                Calendar c = Calendar.getInstance();
                c.set(año, mes, dia, hora, minutos);

                if (!c.before(Calendar.getInstance())) {
                    intent.putExtra("Fecha", c);
                    intent.putExtra("Local", tipoPartidoSeleccionado.Local);
                    intent.putExtra("Visitante", tipoPartidoSeleccionado.Visitante);
                    intent.putExtra("DatabaseID", tipoPartidoSeleccionado.DataBaseID);
                    startActivity(intent);
                } else
                    Toast.makeText(context, "No se puede cambiar un partido anterior a hoy", Toast.LENGTH_LONG).show();
                return true;

            case R.id.borrar_partido:
                if (tipoPartidoSeleccionado.AutorID.equals(mAuth.getUid())) {
                    myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).removeValue();
                    Toast.makeText(context, "Partido borrado correctamente", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(context, IntroducirResultadoPartido.class));
                    finish();
                } else
                    Toast.makeText(context, "Únicamente puedes borrar los partidos creados por ti", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.marcador_electronico:

                return true;


            default:
                return super.onContextItemSelected(item);
        }
    }
}
