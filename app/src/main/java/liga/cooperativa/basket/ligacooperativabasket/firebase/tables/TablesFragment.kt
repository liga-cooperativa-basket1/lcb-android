package liga.cooperativa.basket.ligacooperativabasket.firebase.tables

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import liga.cooperativa.basket.ligacooperativabasket.R

class TablesFragment : Fragment() {

    private lateinit var tableAdapter: TableAdapter
    private lateinit var viewPager: ViewPager2
    private val tablesViewModel by lazy { ViewModelProvider(this)[TablesViewModel::class.java] }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tables, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tablesViewModel.groupSize.observe(viewLifecycleOwner) {
            tableAdapter = TableAdapter(this, it)
            viewPager = view.findViewById(R.id.pager)
            viewPager.adapter = tableAdapter
            val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = "GRUPO ${(position + 1)}"
            }.attach()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = TablesFragment()
    }
}
