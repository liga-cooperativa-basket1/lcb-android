package liga.cooperativa.basket.ligacooperativabasket.firebase;


import android.content.Intent;
import android.os.Bundle;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoCancha;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoFase;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;
import liga.cooperativa.basket.ligacooperativabasket.firebase.utils.MaterialDialogsKt;
import liga.cooperativa.basket.ligacooperativabasket.listados.AdaptadorListas;


public class ElegirCanchaPartido extends BaseActivity {

    private static ListView listViewElegirCancha;
    private static ArrayList<TipoPartido> listaTipoPartidos = new ArrayList<>();
    private static ArrayList<TipoCancha> listaTipoCanchas = new ArrayList<>();
    private static ArrayList<TipoCancha> canchasLibres = new ArrayList<>();
    private Calendar fechaElegida = Calendar.getInstance();
    private String equipoLocal;
    private String equipoVisitante;
    private String databaseID;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            startActivity(new Intent(context, FijarPartidoRival.class));
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_fijar_rival);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        FloatingActionButton addButton = findViewById(R.id.addButton);
        addButton.setVisibility(View.VISIBLE);

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(user.getEmail());
        textViewNombreUsuario.setText(user.getDisplayName());

        fechaElegida = (Calendar) getIntent().getExtras().getSerializable("Fecha");
        equipoLocal = getIntent().getExtras().getString("Local");
        equipoVisitante = getIntent().getExtras().getString("Visitante");
        databaseID = getIntent().getExtras().getString("DatabaseID");




        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                showProgressDialog("Actualizando canchas disponibles...");

                canchasLibres = new ArrayList<>();
                listaTipoCanchas = new ArrayList<>();
                listaTipoPartidos = new ArrayList<>();

                iterable = dataSnapshot.child("Partidos").getChildren();
                for (DataSnapshot ds : iterable) {
                    TipoPartido p = ds.getValue(TipoPartido.class);
                    listaTipoPartidos.add(p);
                }

                iterable = dataSnapshot.child("Canchas").getChildren();
                for (DataSnapshot ds : iterable) {
                    TipoCancha c = ds.getValue(TipoCancha.class);
                    listaTipoCanchas.add(c);
                }


                canchasLibres = TipoCancha.obtenerCanchasLibresFecha(fechaElegida, listaTipoPartidos, listaTipoCanchas);
                canchasLibres = TipoCancha.eliminarDuplicados(canchasLibres);


                listViewElegirCancha = findViewById(R.id.listaCalendario);


                listViewElegirCancha.setAdapter(new AdaptadorListas(context, R.layout.elem_lista_elegircancha, canchasLibres) {
                    @Override
                    public void onEntrada(Object entrada, View view) {
                        if (entrada != null) {
                            try {
                                TipoCancha tipoCancha = (TipoCancha) entrada;

                                TextView textViewNombre = view.findViewById(R.id.nombre_cancha);
                                if (textViewNombre != null)
                                    textViewNombre.setText(tipoCancha.Nombre);

                                TextView textViewDirección = view.findViewById(R.id.direccion_cancha);
                                if (textViewDirección != null)
                                    textViewDirección.setText(tipoCancha.Direccion);

                                TextView porcentaje_uso = view.findViewById(R.id.porcentaje_uso);
                                if (porcentaje_uso != null) {
                                    DecimalFormat df = new DecimalFormat("#0.00");
                                    porcentaje_uso.setText("" + df.format(tipoCancha.PorcentajeUso) + "% de los partidos jugados");
                                }

                            } catch (Exception ex) {
                                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

                listViewElegirCancha.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int posición, long l) {
                        TipoFase fase = dataSnapshot.child("Fase").getValue(TipoFase.class);
                        TipoPartido tipoPartido = new TipoPartido(fechaElegida, canchasLibres.get(posición).Nombre, equipoLocal, equipoVisitante, mAuth.getUid(), fase);
                        if (databaseID != null)
                            myRef.child("Partidos").child(databaseID).setValue(tipoPartido);
                        else
                            myRef.child("Partidos").push().setValue(tipoPartido);
                        startActivity(new Intent(context, MainActivity.class));
                    }
                });

                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        addButton.setOnClickListener(view -> {
            MaterialDialog materialDialog = new MaterialDialog(context, MaterialDialog.getDEFAULT_BEHAVIOR());
            MaterialDialogsKt.showInput(materialDialog, "Nombre de la cancha", text -> {
                TipoCancha nuevaCancha = new TipoCancha(text);
                myRef.child("Canchas").push().setValue(nuevaCancha);
                return null;
            });
        });
    }
}
