package liga.cooperativa.basket.ligacooperativabasket.firebase.tables

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import liga.cooperativa.basket.ligacooperativabasket.firebase.Clasificacion
import liga.cooperativa.basket.ligacooperativabasket.firebase.Clasificacion.GROUP

class TableAdapter(fragment: Fragment, val size: Int) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = size

    override fun createFragment(position: Int): Fragment {
        val fragment = Clasificacion()
        fragment.arguments = Bundle().apply {
            putInt(GROUP, position)
        }
        return fragment
    }
}
