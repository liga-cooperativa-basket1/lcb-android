package liga.cooperativa.basket.ligacooperativabasket.firebase

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import liga.cooperativa.basket.ligacooperativabasket.BuildConfig
import liga.cooperativa.basket.ligacooperativabasket.R
import liga.cooperativa.basket.ligacooperativabasket.network.GitlabClient
import liga.cooperativa.basket.ligacooperativabasket.network.GitlabRelease
import mehdi.sakout.aboutpage.AboutPage
import mehdi.sakout.aboutpage.Element

class InfoActivity : AppCompatActivity() {

    private var mainViewModel: InfoViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel = ViewModelProvider(this)[InfoViewModel::class.java]

        val aboutPage = AboutPage(this)
            .setDescription("Liga Cooperativa de Baloncesto de Madriz")
            .setImage(R.mipmap.ic_launcher)
            .addEmail("ligabasketcooperativa@gmail.com ")
            .addWebsite("https://www.ligabasketcooperativa.org")
            .addGroup("Versión: " + BuildConfig.VERSION_NAME)
            .addItem(
                Element(
                    "Comprobar actualizaciones",
                    R.drawable.download
                ).apply {
                    onClickListener = View.OnClickListener { checkForUpdates() }
                }
            )
            .addWebsite(GitlabClient.repoUrl, "Ver código fuente")
            .create()
        setContentView(aboutPage)
    }

    private fun checkForUpdates() {
        mainViewModel?.newVersion?.observe(this) { releaseName: GitlabRelease? ->
            releaseName?.let {
                updateDialog(it).show()
            } ?: Toast.makeText(this, "No hay actualizaciones", Toast.LENGTH_SHORT).show()
        }
        var info: PackageInfo? = null
        try {
            info = packageManager.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        mainViewModel?.checkUpdates(info)
    }

    private fun updateDialog(newVersion: GitlabRelease): AlertDialog {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Version ${newVersion.name} disponible")
            .setMessage("Existe una nueva versión. ¿Quiere actualizarla?")
            .setPositiveButton("Aceptar") { _, _ ->
                val uri = Uri.parse(
                    GitlabClient.repoUrl +
                        newVersion.description.substringAfterLast("(")?.substringBeforeLast(")")
                )
                val browserIntent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(browserIntent)
            }
            .setNegativeButton("Cancelar") { _: DialogInterface?, _: Int -> }
        return builder.create()
    }
}
