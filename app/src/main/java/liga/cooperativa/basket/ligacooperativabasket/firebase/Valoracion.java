package liga.cooperativa.basket.ligacooperativabasket.firebase;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import liga.cooperativa.basket.ligacooperativabasket.R;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoEquipo;
import liga.cooperativa.basket.ligacooperativabasket.firebase.tipos.TipoPartido;

public class Valoracion extends BaseActivity {

    ImageButton positivo;
    ImageButton negativo;
    ImageButton neutro;
    TipoPartido tipoPartidoSeleccionado;
    TextView local, visitante;
    ImageView logo_local, logo_visitante;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_valoraciones);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
//        FirebaseUser user = mAuth.getCurrentUser();

        TextView textViewNombreUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewNombreUsuario);
        TextView textViewEmailUsuario = navigationView.getHeaderView(0).findViewById(R.id.textViewEmailUsuario);

        textViewEmailUsuario.setText(mAuth.getCurrentUser().getEmail());
        textViewNombreUsuario.setText(mAuth.getCurrentUser().getDisplayName());

        positivo = findViewById(R.id.valoracion_positiva);
        negativo = findViewById(R.id.valoracion_negativa);
        neutro = findViewById(R.id.valoracion_neutra);

        local = findViewById(R.id.local);
        visitante = findViewById(R.id.visitante);
        logo_local = findViewById(R.id.logo_local);
        logo_visitante = findViewById(R.id.logo_visitante);

        try {
            tipoPartidoSeleccionado = (TipoPartido) getIntent().getExtras().getSerializable("Partido");
        } catch (Exception ex) {
            hideProgressDialog();
        }


        myRef = FirebaseDatabase.getInstance().getReference();


        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                miEquipoSt = (String) dataSnapshot.child("Usuarios").child(mAuth.getUid()).child("Equipo").getValue();

                iterable = dataSnapshot.child("Equipos").getChildren();
                ArrayList<TipoEquipo> arrayListTipoEquipo = new ArrayList<>();
                TipoEquipo equipoLocal = null, equipoVisitante = null;

                for (DataSnapshot ds : iterable) {
                    TipoEquipo tipoEquipo = ds.getValue(TipoEquipo.class);
                    tipoEquipo.DataBaseID = ds.getKey();
                    arrayListTipoEquipo.add(tipoEquipo);
                }


                for (TipoEquipo te : arrayListTipoEquipo) {
                    if (te.Nombre.equals(tipoPartidoSeleccionado.Local))
                        equipoLocal = te;
                    else if (te.Nombre.equals(tipoPartidoSeleccionado.Visitante))
                        equipoVisitante = te;
                }

                local.setText(equipoLocal.Nombre);
                visitante.setText(equipoVisitante.Nombre);

                /*LOGO LOCAL*/
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageReference = storage.getReference().child(equipoLocal.LinkLogo);

                if (storageReference != null)
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_local);

                /*LOGO VISITANTE*/
                storageReference = storage.getReference().child(equipoVisitante.LinkLogo);

                if (storageReference != null)
                    Glide.with(context).using(new FirebaseImageLoader()).load(storageReference).into(logo_visitante);

                positivo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (miEquipoSt.equals(tipoPartidoSeleccionado.Local))
                            tipoPartidoSeleccionado.Valoracion.Visitante = "1";
                        else
                            tipoPartidoSeleccionado.Valoracion.Local = "1";

                        myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).setValue(tipoPartidoSeleccionado);

                        startActivity(new Intent(context, MainActivity.class));
                        finish();
                    }
                });

                neutro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (miEquipoSt.equals(tipoPartidoSeleccionado.Local))
                            tipoPartidoSeleccionado.Valoracion.Visitante = "0";
                        else
                            tipoPartidoSeleccionado.Valoracion.Local = "0";

                        myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).setValue(tipoPartidoSeleccionado);

                        startActivity(new Intent(context, MainActivity.class));
                        finish();
                    }
                });

                negativo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (miEquipoSt.equals(tipoPartidoSeleccionado.Local))
                            tipoPartidoSeleccionado.Valoracion.Visitante = "-1";
                        else
                            tipoPartidoSeleccionado.Valoracion.Local = "-1";

                        myRef.child("Partidos").child(tipoPartidoSeleccionado.DataBaseID).setValue(tipoPartidoSeleccionado);

                        startActivity(new Intent(context, MainActivity.class));
                        finish();
                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("¿Seguro que desea salir?")
                .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        builder.create();
        builder.show();
    }
}
